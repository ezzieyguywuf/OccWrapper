/*
    OccWrapper, a C++ wrapper for OpenCascade.
    Copyright (C) 2018  Wolfgang E. Sanyer. See LICENSE file for more info.
*/
#include <ModifiedSolids.hpp>
#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include <vector>
#include <utility>
#include <iostream>

using uint = unsigned int;
using std::pair;
using std::vector;
using ::testing::ElementsAreArray;
using ::testing::UnorderedElementsAreArray;

TEST(OccBooleanSolid, getBoolFace)
{
    Occ::Box box(10, 10, 10);
    Occ::Cylinder cyl(2.5, 10);

    // Create the Fusion
    Occ::BooleanSolid boolSolid = Occ::BooleanSolid::makeFusion(box, cyl);

    // Choose a face to get a reference to
    const Occ::Face aFace = box.getNamedFace(Occ::FaceName::front);

    // Get a reference to it
    const Occ::BoolFaceRef ref = boolSolid.getFaceReference(aFace);

    const Occ::Face checkFace = boolSolid.getBoolFace(ref);
    EXPECT_EQ(aFace, checkFace);
}

TEST(OccBooleanSolid, getBoolFace_copy)
{
    Occ::Box box(10, 10, 10);
    Occ::Cylinder cyl(2.5, 10);

    // Create the Fusion
    Occ::BooleanSolid boolSolid = Occ::BooleanSolid::makeFusion(box, cyl);

    // Choose a face to get a reference to
    const Occ::Face& aFace = box.getNamedFace(Occ::FaceName::front);

    // Get a reference to it
    const Occ::BoolFaceRef ref = boolSolid.getFaceReference(aFace);

    // Make a copy of the referenced face
    Occ::Face checkFace = boolSolid.getBoolFace(ref);

    EXPECT_EQ(aFace, checkFace);
}

TEST(OccBooleanSolid, badBoolFaceRef)
{
    Occ::Box box(10, 10, 10);
    Occ::Cylinder cyl(2.5, 10);

    // Create the Fusion
    Occ::BooleanSolid boolSolid = Occ::BooleanSolid::makeFusion(box, cyl);

    Occ::BoolFaceRef ref(1,0,3);
    EXPECT_THROW(boolSolid.getBoolFace(ref), std::out_of_range);
}

TEST(OccBooleanSolid, operatorEqual)
{
    Occ::Box box(10, 10, 10);
    Occ::Cylinder cyl1(2.5, 10);
    Occ::Cylinder cyl2(2.5, 5);

    // Create the Fusion
    Occ::BooleanSolid boolSolid1 = Occ::BooleanSolid::makeFusion(box, cyl1);
    Occ::BooleanSolid boolSolid2 = Occ::BooleanSolid::makeFusion(box, cyl2);

    boolSolid1 = boolSolid2;
    EXPECT_EQ(boolSolid1, boolSolid2);
}

TEST(OccBooleanSolid, accessNonSplitFace){
    Occ::Box box(10, 10, 10);
    Occ::Cylinder cyl(2.5, 10);

    // Create the Fusion
    Occ::BooleanSolid boolSolid = Occ::BooleanSolid::makeFusion(box, cyl);

    // get reference to the original FRONT face on the box - this will be used to find the
    // new FRONT face in the BooleanSolid
    const Occ::Face& origFrontFace = box.getNamedFace(Occ::FaceName::front);

    // Usually, the user will select a face in the fused BooleanSolid, and we would use
    // that as the parameter to BooleanSolid::getFaceReference. In the absense of that
    // functionality for this unit test, we will instead programmatically find the front
    // face.
    const Occ::ModifiedSolid& boxMod = boolSolid.getModifiedSolid(box);
    const Occ::ModifiedFace& frontModFace = boxMod.getModifiedFace(origFrontFace);

    // This assertion should be redundant, as it should be tested for in the ModifiedSolid
    // unit tests, but we'll put it here anyway just to be safe.
    EXPECT_EQ(frontModFace.size(), 1);

    // This is the index in the fused BooleanSolid that points to what was originall our
    // FRONT face in the Occ::Box
    unsigned int i = frontModFace[0];

    // Get a reference to the FRONT face on the fused BooleanSolid
    const Occ::BoolFaceRef fusFrontFaceRef = boolSolid.getFaceReference(boolSolid.getFaces().at(i));

    // Now we need to build up a BoolFaceRef to check against fusFrontFaceRef
    int whichMod = -1;
    unsigned int whichFace;
    unsigned int whichSplit;
    i = 0;
    for (const Occ::ModifiedSolid& aModSolid : boolSolid.getModifiedSolids())
    {
        if (aModSolid == boxMod)
        {
            whichMod = i;
            return;
        }
        i++;
    }
    whichFace = boxMod.getOrigSolid().getFaceIndex(origFrontFace);
    whichSplit = 0;
    EXPECT_EQ(fusFrontFaceRef, Occ::BoolFaceRef(whichMod, whichFace, whichSplit));
}

TEST(OccBooleanSolid, accessSplitFace){
    Occ::Box box(10, 10, 10);
    Occ::Cylinder cyl(2.5, 10);

    // Create the Fusion
    Occ::BooleanSolid boolSolid = Occ::BooleanSolid::makeFusion(box, cyl);

    // The TOP of the Box ond Cylinder will become three co-planar faces.
    const Occ::Face& origTopBoxFace = box.getNamedFace(Occ::FaceName::top);
    const Occ::Face& origTopCylFace = cyl.getNamedFace(Occ::FaceName::top);

    // The user will typically visually select a Face which will then be passed to
    // BooleanSolid::getFaceReference. We will programmatically determine the Face s for
    // which we need a constant reference

    // first get our ModifiedSolid for each constituent
    const Occ::ModifiedSolid& boxMod = boolSolid.getModifiedSolid(box);
    const Occ::ModifiedSolid& cylMod = boolSolid.getModifiedSolid(cyl);

    // Now we gather our split faces - this is equivalent to the user selecting all the
    // TOP faces in the Fusion Solid
    vector<Occ::Face> topFusBoxFaces = boxMod.getModifications(origTopBoxFace);
    vector<Occ::Face> topFusCylFaces = cylMod.getModifications(origTopCylFace);

    // Gather our BoolFaceRef
    Occ::BoolFaceRef box0 = boolSolid.getFaceReference(topFusBoxFaces[0]);
    Occ::BoolFaceRef box1 = boolSolid.getFaceReference(topFusBoxFaces[1]);
    Occ::BoolFaceRef cyl0 = boolSolid.getFaceReference(topFusCylFaces[0]);
    Occ::BoolFaceRef cyl1 = boolSolid.getFaceReference(topFusCylFaces[1]);

    // Gather data for assertions
    unsigned int boxModIndex = boxMod == boolSolid.getModifiedSolids()[0] ? 0 : 1;
    unsigned int cylModIndex = boxModIndex == 0 ? 1 : 0;
    unsigned int boxTopIndex = box.getFaceIndex(box.getNamedFace(Occ::FaceName::top));
    unsigned int cylTopIndex = cyl.getFaceIndex(cyl.getNamedFace(Occ::FaceName::top));

    // Assertions - I know from experiments that box0 and cyl0 should point to the same
    // Face on boolSolid

    EXPECT_EQ(box0, cyl0);
    EXPECT_EQ(box1, Occ::BoolFaceRef(boxModIndex, boxTopIndex, 1));
    EXPECT_EQ(cyl1, Occ::BoolFaceRef(cylModIndex, cylTopIndex, 1));
}

TEST(OccBooleanSolid, getModifiedSolid){
    Occ::Box box(10, 10, 10);
    Occ::Cylinder cyl(2.5, 10);
    Occ::BooleanSolid boolSolid = Occ::BooleanSolid::makeFusion(box, cyl);

    EXPECT_EQ(box, boolSolid.getModifiedSolid(box).getOrigSolid());
    EXPECT_EQ(cyl, boolSolid.getModifiedSolid(cyl).getOrigSolid());
}
