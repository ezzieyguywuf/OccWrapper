/*
    OccWrapper, a C++ wrapper for OpenCascade.
    Copyright (C) 2018  Wolfgang E. Sanyer. See LICENSE file for more info.
*/
#include <Shapes.hpp>
#include <PrimitiveSolids.hpp>
#include <pybind11/pybind11.h>

namespace py = pybind11;

bool accessMethod(PyObject* obj)
{
    py::handle handle(obj);
    try
    {
        Occ::Box* box = handle.cast<Occ::Box*>();
        assert(box != nullptr);
    }
    catch (const py::cast_error& e)
    {
        PyErr_SetString(PyExc_RuntimeError, e.what());
        return false;
    }
    std::cout << "succesfully casted in accessMethod" << std::endl;
    return true;
}

static PyObject* pyAccess(PyObject* /*self*/, PyObject* args)
{
    PyObject* obj;
    if(not PyArg_ParseTuple(args, "O", &obj))
        return NULL;

    if(not accessMethod(obj))
    {
        return NULL;
    }

    Py_INCREF(Py_None);
    return Py_None;
}

static PyMethodDef PyModUserMethods[] = 
{
    {"access", (PyCFunction) pyAccess, METH_VARARGS, "callback?"},
    {NULL, NULL, 0, NULL}
};

PyModuleDef PyModUser = 
{
    PyModuleDef_HEAD_INIT,
    "PyModUser",
    "This is a simple python module...",
    -1,
    PyModUserMethods,
    NULL,
    NULL,
    NULL,
    NULL
};

PyMODINIT_FUNC PyInit_PyModUser(void)
{
    PyObject* m = PyModule_Create(&PyModUser);
    return m;
}

int main(int argc, char* argv[])
{
    (void) argc;
    wchar_t *program = Py_DecodeLocale(argv[0], NULL);
    if(program == NULL)
    {
        std::cerr << "Fatal: cannot decode argv[0]" << std::endl;
        exit(1);
    }
    PyImport_AppendInittab("PyModUser", PyInit_PyModUser);
    Py_SetProgramName(program);
    Py_Initialize();
    PyRun_SimpleString(R"py(
import sys
import os
import PyModUser as py
# We go up one level because cmake runs us in build/test. This is fragile, and can be
# improved in greatly
#target = os.path.join('../', 'src', 'srcPy')
#if(not os.path.exists(target)):
#    raise ValueError("I expected to find " + target)
    
#sys.path.insert(0, target)
import OccWrapper

box = OccWrapper.Box(10, 10, 10)
py.access(box)
print("succesfully sent to access!")
print(OccWrapper.__file__)
)py");

    if(Py_FinalizeEx() < 0)
    {
        exit(120);
    }

    PyMem_RawFree(program);
    return 0;
}
