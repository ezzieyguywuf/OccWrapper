/*
    OccWrapper, a C++ wrapper for OpenCascade.
    Copyright (C) 2018  Wolfgang E. Sanyer. See LICENSE file for more info.
*/
#include <PrimitiveSolids.hpp>
#include <TopoDS_Shape.hxx>
#include <BRepPrimAPI_MakeBox.hxx>
#include <TopExp.hxx>
#include <TopTools_IndexedMapOfShape.hxx>
#include <TopAbs_ShapeEnum.hxx>
#include <TopoDS_Face.hxx>
#include <TopoDS.hxx>
#include "gtest/gtest.h"

TEST(OccSolid, copyEdge)
{
    Occ::Box mySolid(10, 10, 10);
    Occ::Edge anEdge = mySolid.getEdges().at(0);
    Occ::Edge copiedEdge = anEdge;

    EXPECT_EQ(anEdge, copiedEdge);
}

TEST(OccSolid, BoxToString)
{
    Occ::Box box(10, 10, 10);
    std::string data = box.toString();
    EXPECT_FALSE(data.empty());
}

TEST(OccSolid, BoxFromString)
{
    Occ::Box box(10, 10, 10);
    std::string data = box.toString();

    Occ::Box check = Occ::Box::fromString(data);
    EXPECT_NE(box, check);
    EXPECT_TRUE(box.overlaps(check));
}

TEST(OccSolid, BoxFromString_invalidString)
{
    EXPECT_THROW(Occ::Box::fromString("a box?"), std::invalid_argument);
}

TEST(OccSolid, BoxFromString_corruptData)
{
    // First, get the data
    Occ::Box box(10, 10, 10);
    std::string data = box.toString();

    // Now we have to deconstruct it a bit
    std::stringstream stream;
    stream << data;
    std::string head;
    stream >> head;

    // Let's recreate the string with purosefully bad data
    std::stringstream newData;
    newData << head << "\n" << 10 << " " << 10;

    // Not enough data
    EXPECT_THROW(Occ::Box::fromString(newData.str()), std::invalid_argument);
    std::stringstream().swap(newData);
    newData << head << "\n" << 10 << " !abc";

    // invalid data
    EXPECT_THROW(Occ::Box::fromString(newData.str()), std::invalid_argument);
}

TEST(OccSolid, CylinderToString)
{
    Occ::Cylinder cyl(2.5, 10);
    std::string data = cyl.toString();
    EXPECT_FALSE(data.empty());
}

TEST(OccSolid, CylinderFromString)
{
    Occ::Cylinder cyl(2.5, 10);
    std::string data = cyl.toString();

    Occ::Cylinder check = Occ::Cylinder::fromString(data);
    EXPECT_NE(cyl, check);
    EXPECT_TRUE(cyl.overlaps(check));
}

TEST(OccSolid, CylinderFromString_invalidString)
{
    EXPECT_THROW(Occ::Cylinder::fromString("this is a cylinder"), std::invalid_argument);
}

TEST(OccSolid, CylinderFromString_corruptData)
{
    // First, get the data
    Occ::Cylinder cyl(2.5, 5);
    std::string data = cyl.toString();

    // Now we have to deconstruct it a bit
    std::stringstream stream;
    stream << data;
    std::string head;
    stream >> head;

    // Let's recreate the string with purosefully bad data
    std::stringstream newData;
    newData << head << "\n" << 10;

    // Net enough data
    EXPECT_THROW(Occ::Cylinder::fromString(newData.str()), std::invalid_argument);

    std::stringstream().swap(newData);
    newData << head << "\n" << 10 << " !abc";

    // invalid data
    EXPECT_THROW(Occ::Cylinder::fromString(newData.str()), std::invalid_argument);
}

TEST(OccPrimitive, getNamedFace)
{
    // Retrieve a Face by name
    Occ::Box box(10, 10, 10);
    Occ::Face face = box.getNamedFace(Occ::FaceName::top);

    // Make a box with the same dimensions to check the return value of getNamedFace -
    // we'll build it manually
    BRepPrimAPI_MakeBox checkBox(10, 10, 10);
    checkBox.Build();
    TopoDS_Face topFace = checkBox.TopFace();
    Occ::Face checkFace(topFace);
    EXPECT_TRUE(face.overlaps(checkFace));
}

TEST(OccPrimitive, castNamedFaceToTopoDS_Face)
{
    // Retrieve a Face by name
    Occ::Box box(10, 10, 10);
    Occ::Face face = box.getNamedFace(Occ::FaceName::top);

    TopoDS_Face aFace(TopoDS::Face(face.getShape()));
    EXPECT_FALSE(aFace.IsNull());
}
