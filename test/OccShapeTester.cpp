/*
    OccWrapper, a C++ wrapper for OpenCascade.
    Copyright (C) 2018  Wolfgang E. Sanyer. See LICENSE file for more info.
*/
#include <Shapes.hpp>
#include <PrimitiveSolids.hpp>
#include <TopoDS_Shape.hxx>
#include <BRepPrimAPI_MakeBox.hxx>
#include <BRepTools.hxx>
#include <TopExp.hxx>
#include <TopTools_IndexedMapOfShape.hxx>
#include <TopAbs_ShapeEnum.hxx>
#include <TopoDS.hxx>
#include <TopLoc_Location.hxx>
#include <Geom_Surface.hxx>
#include <gp_Trsf.hxx>
#include <gp_Vec.hxx>
#include <gp_Pnt.hxx>
#include <BRep_Tool.hxx>
#include "gtest/gtest.h"

#include <sstream>

// Helper class - concrete implementation of Occ::Shape.
class MyShape : public Occ::Shape
{
    public:
        MyShape(TopoDS_Shape shape)
            : Occ::Shape(shape){};
};

TEST(OccShape, streamOut)
{
    Occ::Box box(10, 10, 10);

    std::stringstream stream;
    EXPECT_TRUE(stream << box);
}

TEST(OccShape, constructNull)
{
    Occ::Box box(10, 10, 10);
    TopoDS_Edge nullEdge(TopoDS::Edge(box.getEdges()[0].getShape()));
    TopoDS_Face nullFace(TopoDS::Face(box.getFaces()[0].getShape()));
    TopoDS_Solid nullSolid(TopoDS::Solid(box.getShape()));

    nullEdge.Nullify();
    nullFace.Nullify();
    nullSolid.Nullify();

    EXPECT_THROW(Occ::Edge anEdge(nullEdge), std::invalid_argument);
    EXPECT_THROW(Occ::Face aFace(nullFace), std::invalid_argument);
    EXPECT_THROW(Occ::Solid aSolid(nullSolid), std::invalid_argument);
}

TEST(OccShape, Equals){
    // Even though these are the same size, and overlap each other, they are topologically
    // distinct because they were made using distinct sets of TopoDS_Face, etc...
    BRepPrimAPI_MakeBox mkBox1(10.0, 10.0, 10.0);
    BRepPrimAPI_MakeBox mkBox2(10.0, 10.0, 10.0);
    mkBox1.Build();
    mkBox2.Build();
    MyShape shape1(mkBox1.Shape());
    MyShape shape2(mkBox1.Shape());
    MyShape shape3(mkBox2.Shape());
    EXPECT_EQ(shape1, shape2);
    EXPECT_NE(shape1, shape3);
}

TEST(OccShape, overlaps)
{
    // Since these are the same size, and in the same location, they should be
    // geometrically equivalent - in other words, they should overlap
    BRepPrimAPI_MakeBox mkBox1(10.0, 10.0, 10.0);
    BRepPrimAPI_MakeBox mkBox2(10.0, 10.0, 10.0);
    mkBox1.Build();
    mkBox2.Build();
    MyShape shape1(mkBox1.Shape());
    MyShape shape2(mkBox2.Shape());
    EXPECT_TRUE(shape1.overlaps(shape2));
    EXPECT_TRUE(shape2.overlaps(shape1));
}

TEST(OccShape, notOverlaps)
{
    // Since one of the boxes is smaller, they are geometrically distinct.
    BRepPrimAPI_MakeBox mkBox1(10.0, 10.0, 10.0);
    BRepPrimAPI_MakeBox mkBox2(10.0, 10.0, 5.0);
    mkBox1.Build();
    mkBox2.Build();
    MyShape shape1(mkBox1.Shape());
    MyShape shape2(mkBox2.Shape());
    EXPECT_FALSE(shape1.overlaps(shape2));
    EXPECT_FALSE(shape2.overlaps(shape1));
}

TEST(OccShape, notOverlapsTranslate)
{
    // Even though these are the same dimensions, we're going to move one of them, so that
    // they are no longer geometrically equivalent.
    BRepPrimAPI_MakeBox mkBox1(10.0, 10.0, 10.0);
    BRepPrimAPI_MakeBox mkBox2(10.0, 10.0, 10.0);
    mkBox1.Build();
    mkBox2.Build();
    MyShape shape1(mkBox1.Shape());
    MyShape shape2(mkBox2.Shape());
    shape2.translate(5,5,5);
    EXPECT_FALSE(shape1.overlaps(shape2));
    EXPECT_FALSE(shape2.overlaps(shape1));
}

TEST(OccShape, translate)
{
    Occ::Box myBox(10, 10, 10); 
    Occ::Face aFace(TopoDS::Face(myBox.getFaces()[0].getShape()));

    aFace.translate(5,5,5);
    gp_Trsf transform;
    gp_Pnt pnt(5,5,5);
    transform.SetTranslation(gp_Vec(5,5,5));
    TopoDS_Face occFace(TopoDS::Face(aFace.getShape()));
    const auto& bottomSurface = BRep_Tool::Surface(occFace);

    // The point at u=0, v=0 (bottom-left) should be at (5,5,5)
    EXPECT_TRUE(bottomSurface->Value(0,0).IsEqual(pnt, Precision::Confusion()));
}

TEST(OccShape, copySolid)
{
    Occ::Box mySolid(10, 10, 10);
    Occ::Box otherSolid(5, 10, 10);
    Occ::Solid copiedSolid = mySolid;
    copiedSolid = otherSolid;

    EXPECT_EQ(otherSolid, copiedSolid);
}

TEST(OccShape, getFaces)
{
    BRepPrimAPI_MakeBox mkBox(10, 10, 10);
    mkBox.Build();
    Occ::Solid myShape(TopoDS::Solid(mkBox.Shape()));
    EXPECT_EQ(myShape.getFaces().size(), 6);
}

TEST(OccShape, getEdges)
{
    BRepPrimAPI_MakeBox mkBox(10, 10, 10);
    mkBox.Build();
    Occ::Solid myShape(TopoDS::Solid(mkBox.Shape()));
    EXPECT_EQ(myShape.getEdges().size(), 12);
}

TEST(OccShape, getFaceIndex)
{
    BRepPrimAPI_MakeBox mkBox(10, 10, 10);
    mkBox.Build();
    Occ::Solid myShape(TopoDS::Solid(mkBox.Shape()));
    Occ::Face front(mkBox.FrontFace());

    EXPECT_GE(myShape.getFaceIndex(front), 0);
    EXPECT_LE(myShape.getFaceIndex(front), 6);
}

TEST(OccShape, getFaceIndexFromCopy)
{
    BRepPrimAPI_MakeBox mkBox(10, 10, 10);
    mkBox.Build();
    Occ::Solid myShape(TopoDS::Solid(mkBox.Shape()));
    TopoDS_Face aFace= mkBox.FrontFace();
    Occ::Face front(aFace);

    EXPECT_GE(myShape.getFaceIndex(front), 0);
    EXPECT_LE(myShape.getFaceIndex(front), 5);
}
