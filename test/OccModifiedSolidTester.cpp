#include <ModifiedSolids.hpp>

#include <array>
#include <map>
#include <vector>
#include "gtest/gtest.h"
#include "gmock/gmock.h"

using std::array;
using std::vector;
using std::map;
using ::testing::ElementsAreArray;
using ::testing::UnorderedElementsAreArray;
using ::testing::IsEmpty;

class OccModifiedSolidTest : public ::testing::Test{
    protected:
        OccModifiedSolidTest()
            : box1(10, 10, 10), 
              box2(5, 10, 10), 
              // static_cast is needed in order to avoid using the
              // ModifiedSolid::(Occ::primitive, Occ::Primitive) constructor on accident,
              // which automatically makes a valid ModifiedSolid
              invalidMod(static_cast<const Occ::Solid&>(box1), static_cast<const Occ::Solid&>(box2)), 
              validMod(static_cast<const Occ::Solid&>(box1), static_cast<const Occ::Solid&>(box2)), 
              origUndefined1(box1.getNamedFace(skip[0])),
              origUndefined2(box1.getNamedFace(skip[1])),
              newUndefined1(box2.getNamedFace(skip[0])),
              newUndefined2(box2.getNamedFace(skip[1]))
        {};
        void SetUp() override{
            for (Occ::FaceName name : box1.getFaceNames())
            {
                const Occ::Face& origFace = box1.getNamedFace(name);
                const Occ::Face& newFace  = box2.getNamedFace(name);

                validMod.addModified(origFace, newFace);
                if (std::find(skip.begin(), skip.end(), name) == skip.end()){
                    invalidMod.addModified(origFace, newFace);
                }
            }
        }
        Occ::Box box1;
        Occ::Box box2;
        Occ::ModifiedSolid invalidMod;
        Occ::ModifiedSolid validMod;
        std::vector<Occ::FaceName> skip = {Occ::FaceName::left, Occ::FaceName::right};
        Occ::Face origUndefined1;
        Occ::Face origUndefined2;
        Occ::Face newUndefined1;
        Occ::Face newUndefined2;
};

//--------------------- Basic constructor and validity tests ------------------

TEST_F(OccModifiedSolidTest, isValid){
    EXPECT_TRUE(validMod.isValid());
    EXPECT_FALSE(invalidMod.isValid());
}

TEST_F(OccModifiedSolidTest, makeFromPrimitive){
    // Build without the static cast that we use in our test fixture
    Occ::ModifiedSolid mod(box1, box2);

    EXPECT_TRUE(mod.isValid());
    EXPECT_THAT(mod.getModified(), ElementsAreArray({0,1,2,3,4,5}));
    EXPECT_THAT(mod.getDeleted(), IsEmpty());
    EXPECT_THAT(mod.getGenerated(), IsEmpty());
}

TEST_F(OccModifiedSolidTest, makeFromPrimitive_invalid){
    Occ::Cylinder cyl(2.5, 10);

    EXPECT_THROW(Occ::ModifiedSolid mod(box1, cyl), std::invalid_argument);
}

//--------------------- Modification Access Tests            ------------------

TEST_F(OccModifiedSolidTest, getModified){
    std::set<uint> validCheck;
    std::set<uint> invalidCheck;
    for(Occ::FaceName name : box1.getFaceNames()){
        uint i = box1.getFaceIndex(box1.getNamedFace(name));
        validCheck.insert(i);
        if (std::find(skip.begin(), skip.end(), name) == skip.end()){
            invalidCheck.insert(i);
        }
    }
    EXPECT_THAT(validMod.getModified(), ElementsAreArray(validCheck));
    EXPECT_THAT(invalidMod.getModified(), ElementsAreArray(invalidCheck));
}

TEST_F(OccModifiedSolidTest, getModifiedResult){
    std::set<uint> check;
    for(Occ::FaceName name : box2.getFaceNames()){
        const Occ::Face& origFace = box1.getNamedFace(name);
        const Occ::Face& newFace  = box2.getNamedFace(name);

        EXPECT_TRUE(validMod.isModified(origFace));
        EXPECT_THAT(validMod.getModifiedFace(origFace).get(), 
                    ElementsAreArray({box2.getFaceIndex(newFace)}));
    }
}

TEST_F(OccModifiedSolidTest, getModifiedResult_splitFace){
    invalidMod.addModified(origUndefined1, newUndefined1);
    invalidMod.addModified(origUndefined1, newUndefined2);

    // We can't predict the order in which the split faces will be stored. Therefore we
    // must generate a list of indices and check that instead.
    set<uint> check = {box2.getFaceIndex(newUndefined1),
                       box2.getFaceIndex(newUndefined2)};

    EXPECT_TRUE(invalidMod.isModified(origUndefined1));
    EXPECT_THAT(invalidMod.getModifiedFace(origUndefined1).get(), 
                ElementsAreArray(check));
}

TEST_F(OccModifiedSolidTest, getDeleted){
    std::set<uint> check;

    for(Occ::FaceName name : skip){
        const Occ::Face& face = box1.getNamedFace(name);
        uint i = box1.getFaceIndex(face);
        invalidMod.addDeleted(face);
        check.insert(i);
    }

    EXPECT_THAT(validMod.getDeleted(), IsEmpty());
    EXPECT_THAT(invalidMod.getDeleted(), ElementsAreArray(check));
}

TEST_F(OccModifiedSolidTest, getGenerated){
    std::set<uint> check;

    for(Occ::FaceName name : skip){
        const Occ::Face& face = box2.getNamedFace(name);
        uint i = box2.getFaceIndex(face);
        invalidMod.addGenerated(face);
        check.insert(i);
    }

    EXPECT_THAT(validMod.getGenerated(), IsEmpty());
    EXPECT_THAT(invalidMod.getGenerated(), ElementsAreArray(check));
}
//--------------------- Modification Description/Check Tests ------------------

TEST_F(OccModifiedSolidTest, isModified){
    const Occ::Face& origFront = box1.getNamedFace(Occ::FaceName::front);
    const Occ::Face& newFront  = box2.getNamedFace(Occ::FaceName::front);
    EXPECT_TRUE(validMod.isModified(origFront));
    EXPECT_FALSE(validMod.isModified(newFront));
}

TEST_F(OccModifiedSolidTest, isDeleted){
    Occ::ModifiedSolid mod = invalidMod;
    mod.addDeleted(origUndefined1);
    mod.addDeleted(origUndefined2);

    EXPECT_TRUE(mod.isDeleted(origUndefined1));
    EXPECT_TRUE(mod.isDeleted(origUndefined2));
    EXPECT_FALSE(mod.isDeleted(newUndefined1));
    EXPECT_FALSE(mod.isDeleted(newUndefined2));
}

TEST_F(OccModifiedSolidTest, isGenerated){
    Occ::ModifiedSolid mod = invalidMod;
    mod.addGenerated(newUndefined1);
    mod.addGenerated(newUndefined2);

    EXPECT_TRUE(mod.isGenerated(newUndefined1));
    EXPECT_TRUE(mod.isGenerated(newUndefined2));
    EXPECT_FALSE(mod.isGenerated(origUndefined1));
    EXPECT_FALSE(mod.isGenerated(origUndefined2));
}

TEST_F(OccModifiedSolidTest, invalidAddGenerated_origDeleted){
    invalidMod.addDeleted(origUndefined1);

    EXPECT_THROW(invalidMod.addModified(origUndefined1, newUndefined1), std::invalid_argument);
}

TEST_F(OccModifiedSolidTest, invalidAddGenerated_newGenerated){
    invalidMod.addGenerated(newUndefined1);

    EXPECT_THROW(invalidMod.addModified(origUndefined1, newUndefined1), std::invalid_argument);
}

TEST_F(OccModifiedSolidTest, invalidAddModified_origNotInShape)
{
    EXPECT_THROW(invalidMod.addModified(newUndefined1, newUndefined2), std::invalid_argument);
}

TEST_F(OccModifiedSolidTest, invalidAddModified_newNotInShape)
{
    EXPECT_THROW(invalidMod.addModified(origUndefined1, origUndefined2), std::invalid_argument);
}

TEST_F(OccModifiedSolidTest, invalidAddDeleted_notInOrigShape)
{
    EXPECT_THROW(invalidMod.addDeleted(newUndefined1), std::invalid_argument);
}

TEST_F(OccModifiedSolidTest, invalidAddDeleted_faceModified)
{
    EXPECT_THROW(validMod.addDeleted(origUndefined1), std::invalid_argument);
    EXPECT_THROW(validMod.addDeleted(origUndefined2), std::invalid_argument);
}

TEST_F(OccModifiedSolidTest, invalidAddGenerated_faceModified)
{
    EXPECT_THROW(validMod.addGenerated(newUndefined1), std::invalid_argument);
    EXPECT_THROW(validMod.addGenerated(newUndefined2), std::invalid_argument);
}
