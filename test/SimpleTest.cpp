#include <ModifiedSolids.hpp>
#include <BRepAlgoAPI_Cut.hxx>
#include <GProp_GProps.hxx>
#include <BRepGProp.hxx>
#include <Precision.hxx>

#include <iostream>

int main(void)
{
    // Make two solids, one wich is a modification of the other
    Occ::Box box1(10, 10, 10);
    Occ::Box box2(10, 10, 5);

    BRepAlgoAPI_Cut cut(box1.getShape(), box2.getShape());
    cut.Build();

    TopoDS_Shape result = cut.Shape();

    std::cout << std::boolalpha;
    std::cout << "box1 == box2 -> " << (box1 == box2) << std::endl;
    std::cout << "box1.IsPartner(box2) -> " << (box1.getShape().IsPartner(box2.getShape())) << std::endl;
    std::cout << "result.IsNull() -> " << result.IsNull() << std::endl;

    // Get volume
    GProp_GProps boxProps, cutProps;
    BRepGProp::VolumeProperties(box1.getShape(), boxProps, true);
    BRepGProp::VolumeProperties(result, cutProps, true);

    std::cout << "box volume = " << boxProps.Mass() << std::endl;
    std::cout << "cut volume = " << cutProps.Mass() << std::endl;
    std::cout << "cut volume = " << (cutProps.Mass() <= Precision::Confusion()) << std::endl;
    std::cout << "confusion = " << Precision::Confusion() << std::endl;
    //std::cout << box1 << std::endl;
    return 0;
}

