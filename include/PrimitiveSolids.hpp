/*
    OccWrapper, a C++ wrapper for OpenCascade.
    Copyright (C) 2018  Wolfgang E. Sanyer. See LICENSE file for more info.
*/
#pragma once

#include <TopoDS.hxx>
#include <BRepBuilderAPI_MakeShape.hxx>
#include <BRepPrimAPI_MakeBox.hxx>
#include <BRepPrimAPI_MakeCylinder.hxx>

#include "Shapes.hpp"

#include <set>

using std::set;

namespace Occ{
    //! Face names for use in Occ::PrimitiveSolid::getNamedFace
    enum class FaceName
    {
        top,
        bottom,
        left,
        right,
        front,
        back,
        lateral,
        INVALID
    };

    //! A Solid in which each Face can be identified by name.
    class Primitive : public Occ::Solid
    {
        /*! This class cannot be instantiated directly, as its only constructor is
         * explicitly deleted. Rather, a developer wishing to extent the Occ::Primitive
         * functionality should inherit Occ::Primitive_ (notice the underscore) and
         * implement the appropriate stuff there.
         */
        public:
            Primitive() = delete;
            virtual ~Primitive() = 0;

            //! Retrieve a Face by name.
            virtual Face getNamedFace(const FaceName& which) const = 0;
            //! Retrieve the list of Occ::FaceName that this class defines.
            virtual const set<FaceName>& getFaceNames() const = 0;

        protected:
            //! Constructor, to be called by derived class.
            Primitive(BRepBuilderAPI_MakeShape& aMaker);
            Primitive(Occ::Solid aShape);

            //! Add a named Face. A helper method for derived classes to populate
            //! myNamedFaces
            void addFace(FaceName name, unsigned int index);
            //! Give derived classes access to myNamedFaces
            const map<FaceName, unsigned int> getNamedFaces() const;

            //! Helper method to reduce code duplication in derived classes
            static std::stringstream checkSentinel(const std::string& data, 
                                                   const std::string& SENTINEL, 
                                                   const std::string& name="");

        private:
            //! Ensure this is a valid Occ::Primitive. Defined in Occ::_Primitive<T>
            virtual void validate() const = 0;

            // member variables
            map<FaceName, unsigned int> myNamedFaces;
    };

    template<class T>
    class _Primitive : public Primitive
    {
        /*  This class utilizes the Curiously Recurring Template Pattern in order to give
         *  each derived class a distinct set of static member variables.
         *
         *  Derived classes should inherit _Primitive rather than Primitive directly. THis
         *  split allows for external users to rely on the public interface of Primitive
         *  without having to worry about passing template parameters. Rather, it is the
         *  responsibility of the developer who is creating a derived class to take care
         *  of all the "template stuff".
         */
        public:
            ~_Primitive() = default;
            Face getNamedFace(const FaceName& which) const override;
            const set<FaceName>& getFaceNames() const override;

        protected:
            _Primitive(BRepBuilderAPI_MakeShape& aMaker);

        private:
            void validate() const override;

            // class variables
            static set<FaceName> myFaceNames;
            static bool validated;
    };


    //! A six-sided Solid of perpendicular Face's.
    class Box : public Occ::_Primitive<Box>
    {
        public:
            //! Constructor, makes a box with the given dimensions. The bottom left corner
            //! is on the origin.
            Box(float dx, float dy, float dz);
            ~Box() = default;
            //! Create a Box from a string.
            static Box fromString(const std::string& data);
            std::string toString() const override;

        private:
            static const std::string SENTINEL;
            float x,y,z;

            //! Constructor
            Box(BRepPrimAPI_MakeBox&& aMaker);
    };

    //! A Cylinder with a Top Face, a Bottom Face, and a Lateral Face.
    class Cylinder : public Occ::_Primitive<Cylinder>
    {
        public:
            /*  Create a cylinder with the given radius and height.
             */
            Cylinder(float radius, float height);
            ~Cylinder() = default;
            static Cylinder fromString(const std::string& data);

            std::string toString() const override;

        private:
            static const std::string SENTINEL;
            Cylinder(BRepPrimAPI_MakeCylinder&& aMaker);

            float myRadius, myHeight;
    };

    // ------ Template Methods must be defined in a header --------------------

    template<class T>
    _Primitive<T>::_Primitive(BRepBuilderAPI_MakeShape& aMaker)
        : Primitive(aMaker)
    {}

    template<class T>
    bool _Primitive<T>::validated = false;

    /*! \returns A reference to the Face which corresponds to the name \p which
     */
    template<class T>
    Face _Primitive<T>::getNamedFace(const FaceName& which) const
    {
        // This way, we avoid unecessary validation checks
        if(!validated){
            this->validate(); // will throw if not valid
            validated = true;
        }
        unsigned int i = this->getNamedFaces().at(which);
        return this->getFaces().at(i);
    }

    template<class T>
    const set<FaceName>& _Primitive<T>::getFaceNames() const
    {
        return myFaceNames;
    }

    /*! \throws std::runtime_error If all Face's in \p aSolid are not named
     *  \throws std::runtime_error If any of the Named Face's are not defined in
     *          myNamedFaces, which is a static member variable of each derived class
     */
    template<class T>
    void _Primitive<T>::validate() const
    {
        const map<FaceName, unsigned int>& namedFaces = this->getNamedFaces();
        if (namedFaces.size() != this->getFaces().size())
        {
            throw std::runtime_error("All Face's in an Occ::Primitive must be associated with a FaceName.");
        }
        for (FaceName name : myFaceNames)
        {
            const auto& search = namedFaces.find(name);
            if (search == namedFaces.end()){
                throw std::runtime_error("Each Named Face in Occ::Primitive _must_ be one of myFaceNames. Did you define this static member variable in your inherited class?");
            }
        }
    }

} // namespace Occ
