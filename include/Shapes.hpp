/*
    OccWrapper, a C++ wrapper for OpenCascade.
    Copyright (C) 2018  Wolfgang E. Sanyer. See LICENSE file for more info.
*/
#pragma once

#include <TopoDS_Shape.hxx>
#include <TopoDS_Edge.hxx>
#include <TopoDS_Face.hxx>
#include <TopoDS_Solid.hxx>
#include <TopoDS_Compound.hxx>
#include <Precision.hxx>
#include <BRep_Builder.hxx>
#include <BRepTools.hxx>

#include <vector>
#include <iostream>
#include <string>
#include <map>
#include <utility> // for std::pair


using std::vector;
using std::vector;
using std::pair;
using std::map;

namespace Occ{
    class Edge;
    class Face;
    //! The base class for all Topological entities (OpenCascade [TopoDS_Shape](https://www.opencascade.com/doc/occt-7.3.0/refman/html/class_topo_d_s___shape.html))
    class Shape
    {
        friend class Solid;
        public:
            virtual ~Shape() = 0;

            //! Retrieve all the Edge in the Shape
            vector<Edge> getEdges() const;
            //! Retrieve all the Face in the Shape
            vector<Face> getFaces() const;
            //! Retrieve the OpenCascade [TopoDS_Shape](https://www.opencascade.com/doc/occt-7.3.0/refman/html/class_topo_d_s___shape.html)
            TopoDS_Shape getShape() const;

            //! Check for Topological equality.
            bool operator==(const Shape& aShape) const;
            //! Check for Topological inequality.
            bool operator!=(const Shape& aShape) const;
            //! Check for Topological equality, with different orientation
            bool isFlipped(const Shape& aShape) const;
            //! Check for geometric equality
            bool overlaps(const Shape& aShape) const;
            //! Check if the TopoDS_Shape we're storing is Null
            bool isNull() const;
            //! Translate the shape.
            void translate(double dx, double dy, double dz);
            //! Creates a brep file of the stored TopoDS_Shape object.
            void writeFile(const std::string filename) const;
            //! Returns a string representation, in brep format, of the TopoDS_Shape
            //! object. Sub-classes can implement a constructor that takes a string if
            //! they wish to support this feature.
            virtual std::string toString() const;

        protected:
            //! This derived classes _must_ call on of our other constructors
            Shape() = delete;
            //! Constructor, but only usable by derived classes.
            Shape(TopoDS_Shape aShape);
            Shape(const std::string& data);


        private:
            TopoDS_Shape shapeFromString(const std::string& data);
            TopoDS_Shape myShape;
    };

    //! Wrapper for OpenCascade's [TopoDS_Edge](https://www.opencascade.com/doc/occt-7.3.0/refman/html/class_topo_d_s___edge.html) class.
    class Edge : public Shape
    {
        public:
            //! Constructor.
            Edge(const TopoDS_Edge& aEdge);
            Edge(const std::string& data);
            ~Edge() = default;
    };

    //! A collection of Occ::Edge's. Akin to OpenCascade's [TopoDS_Face](https://www.opencascade.com/doc/occt-7.3.0/refman/html/class_topo_d_s___face.html)
    class Face : public Shape
    {
        public:
            //! Constructor.
            Face(const TopoDS_Face& aFace);
            Face(const std::string& data);
            ~Face() = default;

            //! Check if two Face's share a common Edge, using Occ::Edge::isSimilar()
            bool sharesEdge(const Face& aFace) const;
            //! Check if this Face contains a particular Edge
            bool containsEdge(const Edge& anEdge) const;
            //! Returns the common Edge that these two Face's share.
            Edge getCommonEdge(const Face& aFace) const;
    };

    //! Base class for all %Solids. Akin to OpenCascade's [TopoDS_Solid](https://www.opencascade.com/doc/occt-7.3.0/refman/html/class_topo_d_s___solid.html)
    class Solid : public Shape
    {
        public:
            //! Constructor.
            Solid(const TopoDS_Shape& aShape);
            //! Constructor.
            Solid(const TopoDS_Solid& aSolid);
            //! Constructor.
            Solid(const TopoDS_Compound& aCompound);
            Solid(const std::string& data);

            ~Solid() = default;

            //! @returns `true` if \a aFace is within \a mySolid
            bool hasFace(const Occ::Face& aFace) const;
            //! Return the index in the vector returned by getFaces() that corresponds to `aFace`
            unsigned int getFaceIndex(const Occ::Face& aFace) const;

        protected:
            //! Default constructor
            Solid();
    };

    std::ostream& operator<<(std::ostream& stream, const Occ::Shape& aShape);
} // namespace Occ

