/*
    OccWrapper, a C++ wrapper for OpenCascade.
    Copyright (C) 2018  Wolfgang E. Sanyer. See LICENSE file for more info.
*/
#pragma once

#include "Shapes.hpp"
#include "PrimitiveSolids.hpp"

#include <BRepAlgoAPI_BooleanOperation.hxx>

#include <utility> // for std::pair
#include <array>
#include <map>
#include <iostream>
#include <initializer_list>

using std::vector;
namespace Occ
{
    //! A helper class for tracking changes to an Occ::Face
    class ModifiedFace
    {
        public:
            ModifiedFace() = default;
            ModifiedFace(const std::initializer_list<unsigned int>& init);

            // Rule of Three

            // These operators are needed in order to use ModifiedFace in std::set.
            // Probably other stl containers too...
            //bool operator< (const ModifiedFace& aModFace) const;
            bool operator== (const ModifiedFace& aModFace) const;

            /** Add the index of a split face
             *  
             *  \warning if \p newIndex has already been added, then nothing happens
             */
            void push_back(unsigned int newIndex);

            //! Returns returns the index located at \p i.
            //! \throws std::out_of_range if size() <= i.
            unsigned int& operator[](unsigned int i);
            const unsigned int& operator[](unsigned int i) const;

            //! This will allow our ModifiedFace to be used in for(:) ("range-based"
            //! for-loop). It will loop all of our tracked splitIndices
            vector<unsigned int>::iterator begin();
            vector<unsigned int>::const_iterator begin() const;
            vector<unsigned int>::iterator end();
            vector<unsigned int>::const_iterator end() const;

            //! Returns true if the original face has been split. This is a convenience
            //! for `size() > 1 ? true : false;`
            bool isSplit() const;

            //! Returns the number of elements, i.e. "splits"
            unsigned int size() const;
            
            const vector<unsigned int>& get() const;
        private:
            vector<unsigned int> mySplitIndices;
    };

    //! A helper class for tracking changes to an Occ::Solid.
    class ModifiedSolid
    {
        public:
            //! Constructor. Results in an invalid Occ::ModifiedSolid
            ModifiedSolid(const Solid& origSolid, const Solid& newSolid);
            //! Constructor, convenience for Occ::Primitive. Results in a valid Occ::ModifiedSolid
            //! \throws std::invalid_argument if both Primitives don't have the same list
            //!         of Named Faces
            ModifiedSolid(const Occ::Primitive& origSolid,
                          const Occ::Primitive& newSolid);

            // Rule of 3
            ModifiedSolid(const Occ::ModifiedSolid& aModifiedSolid);
            ModifiedSolid& operator=(const Occ::ModifiedSolid& aModifiedSolid);
            ~ModifiedSolid() = default;

            /*! @returns `true` if this ModifiedSolid is valid. A valid ModifiedSolid is
             *            one in which each face in the original and modified Occ::Solid
             *            have been fully accounted for
             */
            bool isValid() const;

            //! Checks for equality.
            bool operator==(const ModifiedSolid& aModifiedSolid) const;
            //! Checks for inequality.
            bool operator!=(const ModifiedSolid& aModifiedSolid) const;

            //! Returns a reference to the modified solid.
            const Solid& getNewSolid() const;
            //! Returns a reference to the unmodified solid.
            const Solid& getOrigSolid() const;

            /** @name Modification Description Functions
             *
             * Use these methods to either define the modification from \a origSolid to
             * \a newSolid or to determine whether a particular Occ::Face has been
             * modified, deleted, or generated.
             */
            //@{
            /*! @brief Identifies \a newFace as a modification of \a origFace
             *
             *  @throws std::invalid_argument if \a origFace is not an Occ::Face of \a
             *          myOrigSolid
             *  @throws std::invalid_argument if \a newFace is not an Occ::Face of \a
             *          myNewSolid
             *  @throws std::invalid_argument if \a origFace has been defined as a deleted
             *          face - it cannot be both.
             *  @throws std::invalid_argument if \a newFace has been defined as a
             *          generated face - it cannot be both.
             */
            void addModified(const Occ::Face& origFace, const Occ::Face& newFace);
            /*! @brief Identifies \a origFace as a deleted Occ::Face in the new Occ::Solid
             *         - it is not present in the new Occ::Solid
             *
             *  @throws std::invalid_argument if \a origFace is not an Occ::Face of \a
             *          myOrigSolid
             *  @throws std::invalid_argument if \a origFace has been defined as a
             *          modified face - it cannot be both.
             */
            void addDeleted(const Occ::Face& origFace);
            /*! @brief Identifies \a newFace as a generated Occ::Face - it was not present
             *         in the original Occ::Solid.
             *
             *  @throws std::invalid_argument if \a newFace is not an Occ::Face of \a
             *          myNewSolid
             *  @throws std::invalid_argument if \a newFace has been defined as a
             *          generated face - it cannot be both.
             */
            void addGenerated(const Occ::Face& newFace);
            /*! @returns true if \a origFace has been modified
             */
            bool isModified(const Occ::Face& origFace) const;
            /*! @returns `true` if \a newFace is the result of a modification, `false`
             *           otherwise
             *
             *  @throws std::invalid_argument if \a newFace is not a face in the new
             *          Occ::Solid
             */
            bool isModification(const Occ::Face& newFace) const;
            /*! @returns true if \a origFace has been deleted, i.e. it _is_ present in the
             *           original Occ::Salid but it _is not_ present in the new Occ::Solid
             */
            bool isDeleted(const Occ::Face& origFace) const;
            /*! @returns true if \a newFace has been generated, i.e. it _is not_ present in
             *           the original Occ::Solid but _is_ present in the new Occ::Solid.
             */
            bool isGenerated(const Occ::Face& newFace) const;
            //@}

            /** @name Modification Access Functions
             *
             * Use these methods to get the list of indices of each of the modified,
             * deleted, and generated faces.
             *
             * @note it is difficult to return the actual vector/set of
             * modified/deleted/generated faces without created copies of these entities.
             * In order to minimize the memory footprint, we've elected to instead give
             * the user sufficient information (i.e. the indices) in order to get the
             * information they need. We _could_ store a vector of pointers (or
             * std::unique_ptr) to the actual faces, but this makes the use of
             * ModifiedSolid more complicated, as the user must be careful to use proper
             * c++11 move semantics when manipulating their instance.
             */
            //@{
            /*! @returns the set of indices to Occ::Face in the original solid which have
             *           been modified.
             */
            set<unsigned int> getModified() const;
            /*! @returns the Occ::ModifiedFace in the new solid which describes how \a
             *           origFace has changed.
             *
             *  @throws std::invalid_argument if \a origFace is not in \a myOrigSolid
             *  @throws std::out_of_bounds if \a origFace was not modified
             */
            const ModifiedFace& getModifiedFace(const Occ::Face& origFace) const;
            /*! @param origFace the Occ::Face in the original Occ::Solid for which to
             *         retrieve the modification
             *
             *  @returns The vecor<Occ::Face> which describes \a origFace has been
             *           modified into. Note that this must be a vector because the Face
             *           may have been split.
             *  
             *  @throws std::invalid_argument if \a origFace is not a face in the original
             *          Occ::Solid
             *  @throws std::invalid_argument if \a origFace was not modified
             */
            vector<Occ::Face> getModifications(const Occ::Face& origFace) const;
            /*! @returns the set of indices to Occ::Face in the original solid which have
             *           been deleted.
             */
            const set<unsigned int>& getDeleted() const;
            /*! @returns the set of indices to Occ::Face in the new solid which have
             *           been generated.
             */
            const set<unsigned int>& getGenerated() const;
            //@}

        private:
            // Ensure that all faces in \a myOrigSolid and \a myNewSolid have been
            // accounted for
            void validate();

            Solid myOrigSolid;
            Solid myNewSolid;

            // Key = index to Face in origSolid, value = ModiedFace containing indices to
            // Face in newSolid
            map<unsigned int, ModifiedFace> myModifiedFaces;
            set<unsigned int> myDeletedFaces;
            set<unsigned int> myGeneratedFaces;

            bool valid = false;
    };

    class BoolFaceRef
    {
        /*! @brief A Convenience class used by Occ::BooleanSolid
         *
         *  This class is used to hold the information needed in order to fully
         *  identify/locate any of the faces in an Occ::BooleanSolid.
         *   
         *  Consider for a second Occ::Primitive. This class has a getNamedFace method,
         *  which relies on the fact that OpenCasCade BRepPrim_* solid's have "named"
         *  faces. In other words, a Box has a Front, Back, etc.. face, a Cylinder has a
         *  Top, Face, Lateral face, etc...
         *
         *  We need a similar functionality for what we call "Boolean Solids", i.e.
         *  Solid's that are constructed from one or more "Constituent Solids" using a
         *  Boolean Operation. This is what a BoolFaceRef does for us.
         *
         *  Any face on the resultant Boolean Solid can be traced back to one of its
         *  Constituent Solids. In fact, the way Occ::BooleanSolid is defined, it is
         *  constructed from a list of Occ::ModifiedSolid's, each of which describe how a
         *  particular Constituent Solid has been modified to produce the final resultant
         *  solid.
         *
         *  Using this knowledge, one can define a BoolFaceRef. The BoolFaceRef
         *  is composed of three values, which are used to describe the origins of any
         *  given aFace:
         *
         *      whichMod: which of the Modified Solid's does aFace originate from?
         *      whichFace: which Face in the Modified Solid'- origSolid does aFace
         *                 originate from?
         *      whichIndex: which of the one or more Face's in newSolid is \p aFace?
         *
         *  Therefore, a BoolFaceRef can be used to retrieve a face as follows:
         *
         *  ```{.cpp}
         *  Occ::Box box(10, 10, 10);
         *  Occ::Cylinder cyl(2.5, 10);
         *  Occ::BooleanSolid fuse = Occ::BooleanSolid::makeFusion(box, cyl);
         *
         *  // The user would select a face, we'll just pick one
         *  const Occ::Face& origFace = fuse.getFaces().at(1);
         *
         *  // Get the BoolFace for origFace
         *  BoolFaceRef ref = fuse.getFaceReference(origFace);
         *
         *  // This is a demonstration of how BoolFaceRef can be used to retrieve origFace
         *  const Occ::ModifiedSolid mod = fuse.getModifiedSolids().at(ref.getModifiedSolidIndex);
         *  const Occ::Face& face = mod.getOrigSolid().getFaces().at(ref.getSolidFaceIndex);
         *  const Occ::Face& retrievedFace = mod.getModification(face, ref.getSplitFaceIndex);
         *
         *  // This assertion should pass.
         *  assert(origFace == retrievedFace);
         *  ```
         */
        public:
            BoolFaceRef(unsigned int mod, unsigned int face, unsigned int index);
            ~BoolFaceRef() = default;

            bool operator==(const BoolFaceRef& aRef) const;
            bool operator!=(const BoolFaceRef& aRef) const;

            unsigned int getModifiedSolidIndex() const;
            unsigned int getSolidFaceIndex() const;
            unsigned int getSplitFaceIndex() const;

            // This will allow gtest to show a human-readable representation of this.
            friend std::ostream& operator<<(std::ostream& os, const BoolFaceRef& aRef)
            {
                os << "whichMod = " << aRef.getModifiedSolidIndex();
                os <<", whichFace = " << aRef.getSolidFaceIndex();
                os << ", whichSub = " << aRef.getSplitFaceIndex();
                return os;
            }
        private:
            unsigned int whichMod;
            unsigned int whichFace;
            unsigned int whichSub;
    };

    //! A Solid that has been created as the result of a [Boolean Operation](https://en.wikipedia.org/wiki/Boolean_operations_on_polygons)

    /*! This is a helper class to help keep track of changes to Solid's due to Boolean
     * Operations. These changes are tracked by providing a single Occ::ModifiedSolid for
     * each of the Solid's that are involved in the Boolean Operation. By providing one
     * such ModifiedSolid for each Solid that is involved, we have all the information
     * necessary to fully define the resultant Solid.
     */
    class BooleanSolid : public Occ::Solid
    {
        public:
            // Rule of Three
            BooleanSolid(const BooleanSolid& aBool);
            ~BooleanSolid() = default;
            BooleanSolid& operator=(const BooleanSolid& aBool);

            /*! @brief create a fused solid
             */
            static BooleanSolid makeFusion(const Occ::Solid& base, const Occ::Solid& tool);
            /*! Get the ModifiedSolid associated with \p aSolid
             *  @throws std::invalid_argument if \a aSolid is not a constituent solid.
             */
            const Occ::ModifiedSolid& getModifiedSolid(const Occ::Solid& aSolid) const;
            //! Get a std::vector of all the ModifiedSolid's
            const std::vector<Occ::ModifiedSolid>& getModifiedSolids() const;

            /*! @brief Identify a reference for \p aFace that describes where it
             *         originated
             *
             *  FIXME
             *  @warning This algorithm assumes that every face in the Resultant solid is a
             *           modification of a face in one of the constituent solids. If this
             *           is not the case, then we will need to redesign this function!!!
             * \throws std::runtime_error if \p aFace is not in the final Solid created as a result of
             *         the Boolean Operation.
             */
            BoolFaceRef getFaceReference(const Occ::Face& aFace) const;

            //! Retrieve the Face in the "Base Solid" that the \a aRef identifies
            const Face& getBaseFace(const BoolFaceRef& aRef) const;
            //! Retrieve the Face in the final BooleanSolid that \a aRef identifies.
            Face getBoolFace(const BoolFaceRef& aRef) const;
            //TODO implement this - it should search through all off
            //Occ::ModifiedFace::getSplitIndices to find a common edge
            //const Occ::Edge& getEdge(const Occ::Face& face1, const Occ::Face& face2) const;

        private:
            BooleanSolid() = default;
            //! Constructor. 
            //
            /*! Each baseSolid describes how a Constituent Solid was modified
             *  to produce the final resultant solid.
             *
             *  @throws std::runtime_error if all the baseSolid.getNewSolid() do not
             *          match.
             *  @note   this constructor should not be called directly. Rather, the
             *          developer should create a factory method that creates the
             *          necessary information to call this constructor.
             */
            BooleanSolid(const vector<ModifiedSolid>& baseSolids);

            static ModifiedSolid makeModified(const Occ::Solid& solid, BRepAlgoAPI_BooleanOperation& op);

            std::vector<Occ::ModifiedSolid> myBaseSolids;
    };
} // namespace Occ
