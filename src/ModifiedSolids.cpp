/*
    OccWrapper, a C++ wrapper for OpenCascade.
    Copyright (C) 2018  Wolfgang E. Sanyer. See LICENSE file for more info.
*/
#include "ModifiedSolids.hpp"

#include <TopTools_ListOfShape.hxx>
#include <TopTools_ListIteratorOfListOfShape.hxx>
#include <TopAbs_ShapeEnum.hxx>
#include <BRepAlgoAPI_Fuse.hxx>
#include <TopoDS_Solid.hxx>


#include <algorithm> // for std::sort
#include <stdexcept>
#include <vector>
#include <iostream>

using Occ::ModifiedFace;
using Occ::ModifiedSolid;
using Occ::BoolFaceRef;
using std::vector;

/* ========================================================
 *   Occ::ModifiedFace definition
 * ========================================================
 */
/*! \class Occ::ModifiedFace 
 *
 *  This helper class is a convenience - it is a simple wrapper around a
 *  std::vector<unsigned int> and which enforces unique values.
 *
 *  In general, when a Topological Face is Modified, it can change one or more of the
 *  following:
 *  - Size
 *  - Orientation (transformation)
 *  - Split (one-to-many)
 *  - Dissapear (many-to-less)
 *
 *  In order to account for this, the Occ::ModifiedSolid class must maintain a list of
 *  indices which maps each face in the origSolid to zero or more faces in the newSolid,
 *  which the following rules:
 *
 *  - A 1-to-1 mapping means that the Occ::Face simply changed shape or orientation
 *    (pretty common). In fact, given some of the specifics of how OpenCasCade works, the
 *    Occ::Face may be geometrically equivalent after a given boolean operation but still
 *    topologically distinct.
 *  - A 1-to-many mapping means that the Occ::Face was split (and possibly changed change
 *    and/or orientation)
 *  - a 1-to-none mapping means the Occ::Face was deleted - it does not exist in newSolid
 *  - a none-to-1 mapping means the Occ::Face was generated - it did not exist in
 *    origSolid
 *
 *  While this data is 'easy' to keep track of in a simple std::map<uint, vector<uint>>,
 *  this helper class is being used in order to make it more clear what is going on ->
 *  this way, a std::map<uint, ModifiedFace> can be used.
 *
 *  \note Occ::ModifiedFace has no concept of which Occ::Solid these indices are
 *  associated with. It is a shallow wrapper around some grouping of unsigned int. As
 *  such, the user must be cautious to ensure that the appropriate indices are being
 *  passed around.
 */
ModifiedFace::ModifiedFace(const std::initializer_list<unsigned int>& init)
{
    // our custom push_back checks for duplicates
    for(unsigned int i : init)
    {
        mySplitIndices.push_back(i);
    }
}

bool ModifiedFace::operator==(const ModifiedFace& aModFace) const
{
    return mySplitIndices == aModFace.mySplitIndices;
}

void ModifiedFace::push_back(unsigned int newIndex)
{
    for (unsigned int i : mySplitIndices)
    {
        if (i == newIndex)
        {
            throw std::invalid_argument("ModifiedFace can only accept a given index once in push_back: no duplicates!");
        }
    }
    mySplitIndices.push_back(newIndex);
}

unsigned int& ModifiedFace::operator[](unsigned int i)
{
    return mySplitIndices.at(i);
}

const unsigned int& ModifiedFace::operator[](unsigned int i) const
{
    return mySplitIndices.at(i);
}

vector<unsigned int>::iterator ModifiedFace::begin()
{
    return mySplitIndices.begin();
}

vector<unsigned int>::const_iterator ModifiedFace::begin() const
{
    return mySplitIndices.cbegin();
}

vector<unsigned int>::iterator ModifiedFace::end()
{
    return mySplitIndices.end();
}

vector<unsigned int>::const_iterator ModifiedFace::end() const
{
    return mySplitIndices.cend();
}

bool ModifiedFace::isSplit() const
{
    return this->size() > 1;
}

unsigned int ModifiedFace::size() const
{
    return mySplitIndices.size();
}

const vector<unsigned int>& ModifiedFace::get() const
{
    return mySplitIndices;
}

//unsigned int ModifiedFace::at(unsigned int i) const
//{
    //if (i >= this->size())
    //{
        //throw std::out_of_range("We don't have enough elements to return that index.");
    //}
    //return *(std::next(mySplitIndices.begin(), i));
//}

/* ========================================================
 *   Occ::ModifiedSolid definition
 * ========================================================
 */

/*! \class Occ::ModifiedSolid 
 *
 * This class provides the foundation upon which Topological consistency can be
 * built. Any time a topological solid is modified, it should be done so in such a
 * matter that the following key pieces of information are known:
 *
 * 1. Which Face's have been modified, and how they've been modified
 * 2. Which Face's have been deleted. (i.e. they only exist in the unmodified Solid).
 * 3. Which Face's have been generated (i.e. they only exist in the modified Solid).
 *
 * With these three pieces of information, we can ensure that any reference to an
 * Edge, or Face can be kept consistent despite changes to the underlying
 * topological database.
 *
 * This works by assuming that, in a valid Solid, any Edge can be fully defined by the
 * two Face's that are adjacent to it. Thus, by keeping track of how the Face's are
 * evolving, we can be sure that any reference to a given Edge is kept consistent.
 * 
 * Example:
 * ```{.cpp}
 * // Suppose we have these two solids (assume each F is an Occ::Face)
 * origSolid.getFaces(); // = {F1, F2, F3}
 * newSolid.getFaces(); // = {F1a, F3a, F3b}
 *
 * Occ::ModifiedSolid modSolid(origSolid, newSolid);
 *
 * // We could generate an Occ::ModifiedSolid as follows
 * ModifiedFaces& mod = modSolid.getModifiedFaces();
 * mod.add(F1, F1a);
 * mod.add(F3, F3a);
 * mod.add(F3, F3b);
 *
 * DeletedFaces& deleted = modSolid.getDeletedFaces();
 * deleted.add(F2);
 * ```
 * 
 * @param origSolid The original, unmodified Occ::Solid.
 * @param newSolid  The final, modified form of \p origSolid
 *
 * @note When constructed, this ModifiedSolid is invalid. In order to be valid, it must
 * account for every Face in \a origSolid and \a newSolid. That is, each Face must be
 * either modified, deleted, or generated.
 */
ModifiedSolid::ModifiedSolid(const Solid& origSolid, const Solid& newSolid)
    : myOrigSolid(origSolid), 
      myNewSolid(newSolid)
{}


ModifiedSolid::ModifiedSolid(const ModifiedSolid& aModifiedSolid)
    : myOrigSolid(aModifiedSolid.myOrigSolid), 
      myNewSolid(aModifiedSolid.myNewSolid),
      myModifiedFaces(aModifiedSolid.myModifiedFaces),
      myDeletedFaces(aModifiedSolid.myDeletedFaces),
      myGeneratedFaces(aModifiedSolid.myGeneratedFaces)
{}

ModifiedSolid::ModifiedSolid(const Occ::Primitive& origSolid,
                            const Occ::Primitive& newSolid)
    : myOrigSolid(origSolid), 
      myNewSolid(newSolid)
{
    if (origSolid.getFaceNames() != newSolid.getFaceNames()){
        throw std::invalid_argument("Both PrimitiveSolids must have the same list of Named Faces");
    }

    for (const Occ::FaceName& faceName : origSolid.getFaceNames())
    {
        const Occ::Face& origFace = origSolid.getNamedFace(faceName);
        const Occ::Face& newFace  = newSolid.getNamedFace(faceName);
        this->addModified(origFace, newFace);
    }
    this->validate();
}

ModifiedSolid& ModifiedSolid::operator=(const ModifiedSolid& aModifiedSolid)
{
    this->myOrigSolid      = aModifiedSolid.myOrigSolid;
    this->myNewSolid       = aModifiedSolid.myNewSolid;
    this->myModifiedFaces  = aModifiedSolid.myModifiedFaces;
    this->myGeneratedFaces = aModifiedSolid.myGeneratedFaces;
    this->myDeletedFaces   = aModifiedSolid.myDeletedFaces;

    return *this;
}

bool ModifiedSolid::isValid() const
{
    return valid;
}

/*! Two ModifiedSolid's are only equivalent if and only if they share:
 *
 * 1. The same original Solid
 * 2. The same modified Solid
 * 3. The same ModifiedFace's
 * 4. The same DeletedFace's
 * 5. The same GeneratedFace's
 */
bool ModifiedSolid::operator==(const ModifiedSolid& aModifiedSolid) const
{
    if (myOrigSolid != aModifiedSolid.myOrigSolid)
        return false;
    if (myNewSolid != aModifiedSolid.myNewSolid)
        return false;
    if(myModifiedFaces != aModifiedSolid.myModifiedFaces)
        return false;
    if (myGeneratedFaces != aModifiedSolid.myGeneratedFaces)
        return false;
    if (myDeletedFaces != aModifiedSolid.myDeletedFaces)
        return false;
    return true;
}

bool ModifiedSolid::operator!=(const ModifiedSolid& aModifiedSolid) const
{
    return not ((*this) == aModifiedSolid);
}

const Occ::Solid& ModifiedSolid::getNewSolid() const
{
    return myNewSolid;
}

const Occ::Solid& ModifiedSolid::getOrigSolid() const
{
    return myOrigSolid;
}

//----------------- Modification Description Functions ------------------------

void ModifiedSolid::addModified(const Occ::Face& origFace, const Occ::Face& newFace)
{
    unsigned int i = myOrigSolid.getFaceIndex(origFace);
    unsigned int j = myNewSolid.getFaceIndex(newFace);
    if (this->isDeleted(origFace)){
        throw std::invalid_argument("An origFace cannot be both deleted and modified.");
    }
    if (this->isGenerated(newFace)){
        throw std::invalid_argument("A newFace cannot be both generated and modified.");
    }
    // This will create a new modifiedFace if needed
    Occ::ModifiedFace& modFace = myModifiedFaces[i];

    // Add the split
    modFace.push_back(j);

    this->validate();
}

void ModifiedSolid::addDeleted(const Occ::Face& origFace)
{
    if (this->isModified(origFace)){
        throw std::invalid_argument("An origFace cannot be both deleted and modified.");
    }
    unsigned int i = myOrigSolid.getFaceIndex(origFace);
    myDeletedFaces.insert(i);
    this->validate();
}

void ModifiedSolid::addGenerated(const Occ::Face& newFace)
{
    if (this->isModification(newFace)){
        throw std::invalid_argument("A newFace cannot be both a modification and generated.");
    }
    unsigned int j = myNewSolid.getFaceIndex(newFace);
    myGeneratedFaces.insert(j);
    this->validate();
}

bool ModifiedSolid::isModified(const Occ::Face& origFace) const
{
    if (!myOrigSolid.hasFace(origFace)){
        return false;
    }

    unsigned int i = myOrigSolid.getFaceIndex(origFace);

    for(const auto& pair : myModifiedFaces){
        if (pair.first == i) return true;
    }
    return false;
}

bool ModifiedSolid::isModification(const Occ::Face& newFace) const
{
    if(!myNewSolid.hasFace(newFace)){
        return false;
    }

    unsigned int i = myNewSolid.getFaceIndex(newFace);
    for(const auto& pair : myModifiedFaces){
        const Occ::ModifiedFace& modFace = pair.second;
        for (const unsigned int& j : modFace)
        {
            if (i == j) return true;
        }
    }
    return false;
}

vector<Occ::Face> ModifiedSolid::getModifications(const Occ::Face& origFace) const
{
    vector<Occ::Face> outFaces;
    for(unsigned int i : this->getModifiedFace(origFace))
    {
        outFaces.push_back(this->getNewSolid().getFaces().at(i));
    }
    if (outFaces.size() == 0)
    {
        throw std::invalid_argument("This face has not been modified. Cannot find modified face.");
    }
    return outFaces;
}

bool ModifiedSolid::isDeleted(const Occ::Face& origFace) const
{
    if (!myOrigSolid.hasFace(origFace)){
        return false;
    }

    unsigned int i = myOrigSolid.getFaceIndex(origFace);
    return myDeletedFaces.count(i) > 0;
}

bool ModifiedSolid::isGenerated(const Occ::Face& newFace) const
{
    if (!myNewSolid.hasFace(newFace)){
        return false;
    }

    unsigned int i = myNewSolid.getFaceIndex(newFace);
    return myGeneratedFaces.count(i) > 0;
}

set<unsigned int> ModifiedSolid::getModified() const
{
    set<unsigned int> ret;
    for (const auto& pair : myModifiedFaces){
        ret.insert(pair.first);
    }
    return ret;
}

const ModifiedFace& ModifiedSolid::getModifiedFace(const Occ::Face& origFace) const
{
    // throws std::invalid_argument as appropriate.
    unsigned int i = myOrigSolid.getFaceIndex(origFace);
    for (const auto& pair : myModifiedFaces)
    {
        if (i == pair.first)
        {
            return pair.second;
        }
    }
    throw std::invalid_argument("That face does not appear to be a modified face!");
}

const set<unsigned int>& ModifiedSolid::getDeleted() const
{
    return myDeletedFaces;
}

const set<unsigned int>& ModifiedSolid::getGenerated() const
{
    return myGeneratedFaces;
}
//---------------------- Private Metheds -------------------------
void ModifiedSolid::validate()
{
    if (valid){
        // Avoid un-necessary checks.
        return;
    }

    const int size1 = myOrigSolid.getFaces().size();
    const int size2 = myNewSolid.getFaces().size();

    // Tese two vecotors old a booolean for each face in each solid. They must both
    // contain all `true` in order for the validation to pass
    vector<bool> checkOrig(size1, false);
    vector<bool> checkNew(size2, false);

    for (const Occ::Face& origFace : myOrigSolid.getFaces()){
        int i = myOrigSolid.getFaceIndex(origFace);
        if (this->isModified(origFace)){
            checkOrig[i] = true;
        }
        else if (this->isDeleted(origFace)){
            checkOrig[i] = true;
        }
        // Do not need to check Generated, seeing as to how addGenerated will fail with an
        // origFace
    }
    for (const Occ::Face& newFace : myNewSolid.getFaces()){
        int i = myNewSolid.getFaceIndex(newFace);
        if (this->isModification(newFace)){
            checkNew[i] = true;
        }
        else if (this->isGenerated(newFace)){
            checkNew[i] = true;
        }
        // Do not need to check Deleted, seeing as to how addDeleted will fail with a
        // newFace
    }

    bool allOrig = std::find(checkOrig.begin(), checkOrig.end(), false) == checkOrig.end();
    bool allNew  = std::find(checkNew.begin(), checkNew.end(), false) == checkNew.end();

    valid = allOrig && allNew;
}

/* ========================================================
 *   Occ::BoolFaceRef definition
 * ========================================================
 */

BoolFaceRef::BoolFaceRef(unsigned int mod, unsigned int face, unsigned int index)
    : whichMod(mod), whichFace(face), whichSub(index)
{}

bool BoolFaceRef::operator==(const BoolFaceRef& aRef) const
{
    bool check1 = whichMod   == aRef.whichMod;
    bool check2 = whichFace  == aRef.whichFace;
    bool check3 = whichSub == aRef.whichSub;
    return (check1 && check2 && check3);
}

bool BoolFaceRef::operator!=(const BoolFaceRef& aRef) const
{
    return !(*this == aRef);
}

unsigned int BoolFaceRef::getModifiedSolidIndex() const
{
    return whichMod;
}
unsigned int BoolFaceRef::getSolidFaceIndex() const
{
    return whichFace;
}

unsigned int BoolFaceRef::getSplitFaceIndex() const
{
    return whichSub;
}

/* ========================================================
 *   Occ::BooleanSolid definition
 * ========================================================
 */

using Occ::BooleanSolid;
using std::vector;
using Occ::Face;
using Occ::ModifiedSolid;

BooleanSolid::BooleanSolid(const BooleanSolid& aBool)
    : Occ::Solid(aBool), myBaseSolids(aBool.myBaseSolids)
{}

BooleanSolid& BooleanSolid::operator=(const BooleanSolid& aBool)
{
    Occ::Solid::operator=(aBool);
    myBaseSolids = aBool.myBaseSolids;
    return *this;
}

//---------------------- Static Methods --------------------------------------

BooleanSolid BooleanSolid::makeFusion(const Occ::Solid& base, const Occ::Solid& tool)
{
    BRepAlgoAPI_Fuse mkFuse(base.getShape(), tool.getShape());
    ModifiedSolid modBase = BooleanSolid::makeModified(base, mkFuse);
    ModifiedSolid modTool = BooleanSolid::makeModified(tool, mkFuse);
    Occ::Solid newSolid(Occ::Solid(TopoDS::Compound(mkFuse.Shape())));
    Occ::BooleanSolid boolSolid({modBase, modTool});
    return boolSolid;
}

//------------------------ Member Methods -------------------------------------

const ModifiedSolid& BooleanSolid::getModifiedSolid(const Occ::Solid& aSolid) const
{
    for (const ModifiedSolid& checkSolid : this->myBaseSolids)
    {
        if (checkSolid.getOrigSolid() == aSolid)
        {
            return checkSolid;
        }
    }
    throw std::invalid_argument("aSolid was not found as a `getOrigSolid()` of any of `myBaseSolids`.");
}

const vector<ModifiedSolid>& BooleanSolid::getModifiedSolids() const
{
    return myBaseSolids;
}

Occ::BoolFaceRef BooleanSolid::getFaceReference(const Face& aFace) const
{
    unsigned int i = 0;
    for (const ModifiedSolid& modSolid : myBaseSolids)
    {
        unsigned int j = 0;
        for (const Face origFace : modSolid.getOrigSolid().getFaces())
        {
            if (modSolid.isModified(origFace)){
                unsigned int k = 0;
                for (unsigned int index: modSolid.getModifiedFace(origFace))
                {
                    const Face checkFace = modSolid.getNewSolid().getFaces().at(index);
                    if (checkFace.isFlipped(aFace))
                    {
                        return Occ::BoolFaceRef(i, j, k);
                    }
                    k++;
                }
            }
            j++;
        }
        i++;
    }
    throw std::runtime_error("Was unable to find a constituent face.");
}

const Face& BooleanSolid::getBaseFace(const Occ::BoolFaceRef& aRef) const
{
    const ModifiedSolid& mod = this->getModifiedSolids().at(aRef.getModifiedSolidIndex());
    return mod.getOrigSolid().getFaces().at(aRef.getSolidFaceIndex());
}

Face BooleanSolid::getBoolFace(const Occ::BoolFaceRef& aRef) const
{
    const ModifiedSolid& mod = this->getModifiedSolids().at(aRef.getModifiedSolidIndex());
    const Face origFace =  mod.getOrigSolid().getFaces().at(aRef.getSolidFaceIndex());
    const ModifiedFace& modFace = mod.getModifiedFace(origFace);
    unsigned int index = modFace[aRef.getSplitFaceIndex()];
    return mod.getNewSolid().getFaces()[index];
}

//------------------------- Private Methods -----------------------------------

// TODO: Figure out why we have to manually check whether baseSolids is empty - the
// std::vector::at method is supposed to have bounds checking.
BooleanSolid::BooleanSolid(const vector<ModifiedSolid>& baseSolids)
    : Solid(baseSolids.empty() ? throw std::out_of_range("baseSolids must not be empty")
                               : baseSolids.at(0).getNewSolid()), 
       myBaseSolids(baseSolids)
{
    for (const ModifiedSolid& modSolid : baseSolids)
    {
        if (modSolid.getNewSolid() != *this)
        {
            throw std::runtime_error("Each Occ::ModifiedSolid MUST have a newSolid that is the same as *this");
        }
    }
}

ModifiedSolid BooleanSolid::makeModified(const Occ::Solid& solid, BRepAlgoAPI_BooleanOperation& op)
{
    const Occ::Solid& newSolid(op.Shape());
    ModifiedSolid mod(solid, newSolid);

    for (const Occ::Face& occFace : solid.getFaces())
    {
        TopoDS_Face aFace = TopoDS::Face(occFace.getShape());
        
        if (op.IsDeleted(aFace))
        {
            // if deleted, add to myDeletedFaces and move on to the next face
            mod.addDeleted(occFace);
            continue;
        }

        TopTools_ListOfShape modified = op.Modified(aFace);
        TopTools_ListIteratorOfListOfShape iterator(modified);

        for(; iterator.More(); iterator.Next())
        {
            TopoDS_Face modifiedFace = TopoDS::Face(iterator.Value());
            const Occ::Face& occModifiedFace(modifiedFace);
            mod.addModified(occFace, occModifiedFace);
        }

        TopTools_ListOfShape generated = op.Generated(aFace);
        TopTools_ListIteratorOfListOfShape genIterator(generated);

        for (; genIterator.More() ; genIterator.Next())
        {
            TopoDS_Shape genShape = genIterator.Value();
            switch (genShape.ShapeType()) {

            case TopAbs_VERTEX:
            {
                //std::clog << "VERTEX was generated. Doing nothing..." << std::endl;
                break;
            }
            case TopAbs_EDGE:
            {
                //std::clog << "EDGE was generated. Storing." << std::endl;
                break;
            }
            case TopAbs_FACE:
            {
                std::clog << "FACE was generated. Doing nothing..." << std::endl;
                //TopoDS_Face genFace = TopoDS::Face(genFace);
                //unsigned int j = myNewSolid.getFaceIndex(genFace);
                //myNewFaces[i].push_back(j);
                break;
            }
            default:
                throw std::runtime_error("Somehow, something other than a Vertex, Edge, or Face was generated");
                    
            }
        }

        if (modified.Extent() == 0 and generated.Extent() == 0)
        {
            // If not deleted, modified, or generated, then the face is the same in the
            // new solid.  Despite being geometrically equivalent, we must still treat it
            // as modified as it will likely have a new index, i.e. it is Topologically
            // distinct.
            mod.addModified(aFace, aFace);
        }
    }
    return mod;
}
