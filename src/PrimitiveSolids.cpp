/*
    OccWrapper, a C++ wrapper for OpenCascade.
    Copyright (C) 2018  Wolfgang E. Sanyer. See LICENSE file for more info.
*/

#include "PrimitiveSolids.hpp"

#include <BRepPrim_Cylinder.hxx>

using Occ::Primitive;
using Occ::Face;
using Occ::FaceName;
using Occ::Box;
using Occ::Cylinder;

/* ========================================================
 *   Occ::Primitive definition
 * ========================================================
 */

Primitive::Primitive(BRepBuilderAPI_MakeShape& aMaker)
    : Solid(TopoDS::Solid(aMaker.Shape()))
{}

Primitive::Primitive(Occ::Solid aShape)
    : Solid(aShape)
{}

Primitive::~Primitive()
{}
//--------------- Protected Methods --------------------------

void Primitive::addFace(FaceName name, unsigned int index){
    myNamedFaces.emplace(name, index);
}


const map<FaceName, unsigned int> Primitive::getNamedFaces() const
{
    return myNamedFaces;
}

/** \param data the data that is attempting to be parsed
 *  \param the SENTINEL that is expected at the beginning of the data
 *  \param the name of the calling class - will be used in error messages
 */
std::stringstream Primitive::checkSentinel(const std::string& data, 
                                           const std::string& SENTINEL, 
                                           const std::string& name)
{
    std::string msg = "It does not appear that this string "
                      "was constructed using ";
    if(not name.empty())
    {
        msg += name + "::toString.";
    }
    else
    {
        msg += "a member method of this class.";
    }

    if(data.find(SENTINEL) != 0)
    {
        msg += " The SENTINEL value did not match.";
        throw std::invalid_argument(msg.c_str());
    }
    std::string truncated = data.substr(SENTINEL.size());
    std::stringstream out;
    out << truncated;
    return out;
}
/* ========================================================
 *   Occ::_Primitive<Box> definition
 * ========================================================
 */

template<>
set<FaceName> Occ::_Primitive<Box>::myFaceNames({
        FaceName::top,
        FaceName::bottom,
        FaceName::front,
        FaceName::back,
        FaceName::left,
        FaceName::right
});

const std::string Box::SENTINEL("!!!~~~___OCC_PRIMITIVE_BOX___~~~!!!\n");

Box::Box(float dx, float dy, float dz)
    : Box(BRepPrimAPI_MakeBox(dx, dy, dz))
{
    x = dx;
    y = dy;
    z = dz;
}

/** If \p data was not created using Box::toString, then this will likely throw an error.
 *  I say "likely" becasue, if you're careful, you can fake it.
 *
 *  \throws std::invalid_argument if data does not describe an Occ::Box
 */
Box Box::fromString(const std::string& data)
{
    std::stringstream stream = Primitive::checkSentinel(data, Box::SENTINEL, "Box");
    float x, y, z;
    if(not (stream >> x >> y >> z))
    {
        std::string msg = "The data appears to be corrupt - cannot create Occ::Box fromString.";
        throw std::invalid_argument(msg.c_str());
    }
    return Box(x, y, z);
}

std::string Box::toString() const
{
    std::stringstream stream;
    stream << Box::SENTINEL << x << " " << y << " " << z;
    return stream.str();
}

//--------------------- Private Methods -----------------------------

Box::Box(BRepPrimAPI_MakeBox&& aMaker)
    : Occ::_Primitive<Box>(aMaker)
{
    this->addFace(FaceName::top    , Solid::getFaceIndex(Face(aMaker.TopFace())));
    this->addFace(FaceName::bottom , Solid::getFaceIndex(Face(aMaker.BottomFace())));
    this->addFace(FaceName::left   , Solid::getFaceIndex(Face(aMaker.LeftFace())));
    this->addFace(FaceName::right  , Solid::getFaceIndex(Face(aMaker.RightFace())));
    this->addFace(FaceName::front  , Solid::getFaceIndex(Face(aMaker.FrontFace())));
    this->addFace(FaceName::back   , Solid::getFaceIndex(Face(aMaker.BackFace())));
}

/* ========================================================
 *   Occ::_Primitive<Cylinder> definition
 * ========================================================
 */

template<>
set<FaceName> Occ::_Primitive<Cylinder>::myFaceNames({
        FaceName::top,
        FaceName::bottom,
        FaceName::lateral
});

const std::string Cylinder::SENTINEL("!!!~~~___OCC_PRIMITIVE_CYLINDER___~~~!!!\n");

Cylinder::Cylinder(float radius, float height)
    : Cylinder(BRepPrimAPI_MakeCylinder(radius, height))
{
    myRadius = radius;
    myHeight = height;
}

Cylinder Cylinder::fromString(const std::string& data)
{
    std::stringstream stream = Primitive::checkSentinel(data, Cylinder::SENTINEL, "Cylinder");
    float radius, height;
    if(not (stream >> radius >> height))
    {
        std::string msg = "The data appears to be corrupt - cannot create Occ::Cylinder fromString.";
        throw std::invalid_argument(msg.c_str());
    }
    return Cylinder(radius, height);
}

std::string Cylinder::toString() const
{
    std::stringstream stream;
    stream << Cylinder::SENTINEL << myRadius << " " << myHeight;
    return stream.str();
}

//------------------- Private Methods -------------------------------

Cylinder::Cylinder(BRepPrimAPI_MakeCylinder&& aMaker)
    : Occ::_Primitive<Cylinder>(aMaker)
{
    BRepPrim_Cylinder cyl = aMaker.Cylinder();

    this->addFace(FaceName::top     , Solid::getFaceIndex(Occ::Face(cyl.TopFace())));
    this->addFace(FaceName::bottom  , Solid::getFaceIndex(Occ::Face(cyl.BottomFace())));
    this->addFace(FaceName::lateral , Solid::getFaceIndex(Occ::Face(cyl.LateralFace())));
}
