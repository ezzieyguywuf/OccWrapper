/*
    OccWrapper, a C++ wrapper for OpenCascade.
    Copyright (C) 2018  Wolfgang E. Sanyer. See LICENSE file for more info.
*/
#include <pybind11/pybind11.h>
#include <iostream>

#include "ShapesPy.hpp"
#include "PrimitiveSolidsPy.hpp"
#include "ModifiedSolidPy.hpp"

PYBIND11_MODULE(OccWrapper, m)
{
    m.doc() = R"pbdoc(
    OccWrapper
    ----------
    A wrapper to open-cascade, available in Python.
    
    This module is a python wrapper around the OccWrapper c++ library.
    OccWrapper is itself a wrapper around the OpenCasCade CAD kernel."
    )pbdoc";
    // note: the order here matters. a python class must be defined before it can be used
    // in another python class. Thus, OccShape must come before OccEdge must come before
    // OccFace, etc...
    init_OccShapes(m);
    init_OccPrimitives(m);
    init_OccModifiedSolids(m);
}
