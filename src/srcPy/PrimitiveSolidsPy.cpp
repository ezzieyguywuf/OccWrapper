/*
    OccWrapper, a C++ wrapper for OpenCascade.
    Copyright (C) 2018  Wolfgang E. Sanyer. See LICENSE file for more info.
*/
#include "PrimitiveSolidsPy.hpp"

void init_OccPrimitives(py::module& m)
{
    //------------- Define a python version of the Occ:FaceName enum -------------------
    py::enum_<Occ::FaceName>(m, "FaceName")
        .value("top", Occ::FaceName::top)
        .value("bottom", Occ::FaceName::bottom)
        .value("left", Occ::FaceName::left)
        .value("right", Occ::FaceName::right)
        .value("front", Occ::FaceName::front)
        .value("back", Occ::FaceName::back)
        .value("lateral", Occ::FaceName::lateral);
    //------------- Define the python Primitive class -------------------
    py::class_<Occ::Primitive, Occ::Solid>(m, "Primitive")
        .def("getNamedFace", &Occ::Primitive::getNamedFace, "Get the face of a primitive by name.")
        .def("getFaceNames", &Occ::Primitive::getFaceNames, "Get te list of FaceNames that this class defines.")
        .doc() = "A primitive solid. This is a pure virtual base class, and as " 
                 "such cannot be\n instantiated on its own.";

    //------------- Define the python Box class -------------------
    BoxPyClass* boxClass = new BoxPyClass(m, "Box");
    boxClass->
        def(py::init<uint, uint, uint>(),
             py::arg("dx"), py::arg("dy"), py::arg("dz"),
             py::doc(R"pbdoc(Create a box in the +x, +y, +z domain.)pbdoc"));
    boxClass->doc() = "A Box. As such, it has six faces. The OccWrapper.FaceNames "
                      "enum can be\nused to access the following faces: top, bottom, "
                      "left, right, front, back.";

    //------------- Define the python Cylinder class -------------------
    CylinderPyClass* cylClass = new CylinderPyClass(m, "Cylinder");
    cylClass ->
        def(py::init<uint, uint>(),
            py::arg("rad"), py::arg("height"),
            py::doc(R"pbdoc(Create a cylinder in the +x, +y, +z domain.)pbdoc"));
    cylClass->doc() = "A Cylinder. As such, it has three faces. The "
                      "OccWrapper.FaceNames enum can be\nused to access the "
                      "following faces: top, bottom, left, right, front, back.";
}
