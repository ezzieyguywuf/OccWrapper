/*
    OccWrapper, a C++ wrapper for OpenCascade.
    Copyright (C) 2018  Wolfgang E. Sanyer. See LICENSE file for more info.
*/
#pragma once

#include <pybind11/pybind11.h>
#include <Shapes.hpp>

namespace py = pybind11;

using ShapePyClass = py::class_<Occ::Shape>;
ShapePyClass* init_OccShapes(py::module& m);
