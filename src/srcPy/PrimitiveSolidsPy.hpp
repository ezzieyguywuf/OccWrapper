/*
    OccWrapper, a C++ wrapper for OpenCascade.
    Copyright (C) 2018  Wolfgang E. Sanyer. See LICENSE file for more info.
*/
#pragma once

#include <pybind11/pybind11.h>
#include <PrimitiveSolids.hpp>

namespace py = pybind11;

using BoxPyClass = py::class_<Occ::Box, Occ::Primitive>;
using CylinderPyClass = py::class_<Occ::Cylinder, Occ::Primitive>;
void init_OccPrimitives(py::module&);
