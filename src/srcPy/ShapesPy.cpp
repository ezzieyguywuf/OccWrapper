/*
    OccWrapper, a C++ wrapper for OpenCascade.
    Copyright (C) 2018  Wolfgang E. Sanyer. See LICENSE file for more info.
*/
#include "ShapesPy.hpp"

ShapePyClass* init_OccShapes(py::module& m)
{
    py::options options;
    options.disable_function_signatures();
    ShapePyClass* shapeClass = new ShapePyClass(m, "Shape"); 

    //------ Define the python Shape class --------------------
    shapeClass->doc() = R"pbdoc(
        Shape is the base class for all Topological Entities.

        It is akin to OpenCascade's TopoDS_Shape. Indeed, OccWrapper's Shape is merely a
        wrapper around TopoDS_Shape
        )pbdoc";
    shapeClass
        //->def(py::init([](Occ::Shape arg){
                    //return Occ::Shape(arg);}))
        ->def("__eq__", [](const Occ::Shape& s1, const Occ::Shape& s2){
                return s1 == s2;}, 
                py::is_operator(),
                py::arg("aShape"),
                py::doc("Tests whether self and aShape are topologically"
                        "equivalent. Uses\nTopoDS_Shape::IsEqual method from OpenCasCade"))
        .def("__ne__", [](const Occ::Shape& s1, const Occ::Shape& s2){
                return s1 != s2;}, py::is_operator())
        .def("isFlipped", &Occ::Shape::isFlipped,
              py::arg("aShape"),
               py::doc("Tests whether self and aShape are topologically"
                       "similar. Uses\nTopoDS:Shape::IsSimilar method from OpenCasCade."))
        .def("isNull", &Occ::Shape::isNull,
               py::doc("Returns true if the Shape is null, i.e. is managing nullptr"))
        .def("translate", &Occ::Shape::translate,
              py::arg("dx"), py::arg("dy"), py::arg("dz"),
              py::doc("Translate the shape (i.e. move it)."))
        .def("writeFile", 
              (void (Occ::Shape::*)(const char*) const) &Occ::Shape::writeFile,
              py::arg("filename"),
              py::doc("Writes the Shape to a file, using the OpenCasCade brep "
                      "file format."))
        .def("getString", &Occ::Shape::toString,
             py::doc("Creates a string reperesentation of the TopoDS_Shape using "
                     "OpenCasCade brep format")
        )

        ;

    //------ Define the python Edge class --------------------
    py::class_<Occ::Edge, Occ::Shape>(m, "Edge")
        .doc() = "A topological Edge. The underlying C++ code wraps a TopoDS_Edge.";

    //------ Define the python Face class --------------------
    py::class_<Occ::Face, Occ::Shape>(m, "Face")
        .def("getEdges", &Occ::Face::getEdges,
             py::doc("Get the OccWrapper.Edge's that make up this Face."))
        .def("sharesEdge", &Occ::Face::sharesEdge, 
                py::arg("aFace"),
                py::doc("Checks whether self has a common OccWrapper.Edge with aFace"))
        .def("containsEdge", &Occ::Face::containsEdge,
                py::arg("anEdge"),
                py::doc("Checks whether self contains anEdge."))
        .doc() = "A topological Face. The underlying C++ code wraps a TopoDS_Face.";

    //------ Define the python Solid class --------------------
    py::class_<Occ::Solid, Occ::Shape>(m, "Solid")
        .def("getFaces", &Occ::Solid::getFaces,
             py::doc("Returns a tuple containing all the OccWrapper.Face's in "
                     "this Solid."))
        .def("getEdges", &Occ::Solid::getEdges,
             py::doc("Returns a tuple containing all the OccWrapper.Edge's in "
                     "this Solid."))
        .def("getFaceIndex", &Occ::Solid::getFaceIndex,
             py::arg("aFace"),
             py::doc("Returns an index to the tuple returned by `getFaces`"
                     "corresponding to `aFace`."))
        .doc() = "A topological Solid. The underlying C++ code wraps a"
                 "TopoDS_Solid or a\nTopoDS_Compound.";

    return shapeClass;
}
