#pragma once

#include <pybind11/pybind11.h>
#include <ModifiedSolids.hpp>

namespace py = pybind11;

void init_OccModifiedSolids(py::module&);
