/*
    OccWrapper, a C++ wrapper for OpenCascade.
    Copyright (C) 2018  Wolfgang E. Sanyer. See LICENSE file for more info.
*/
#include <pybind11/stl.h>
#include "ModifiedSolidPy.hpp"


#include <map>
#include <vector>

using std::map;
using std::vector;
using uint = unsigned int;

void init_OccModifiedSolids(py::module& m)
{
    //------------ Define python ModifiedSolid class ---------------------
    py::class_<Occ::ModifiedSolid>(m, "ModifiedSolid")
        .def(py::init<Occ::Box, Occ::Box>(),
             py::arg("origBox"), py::arg("newBox"),
             py::doc(R"pydoc(Creates a ModifiedSolid between the two boxes)pydoc"))
        .def(py::init<Occ::Cylinder, Occ::Cylinder>(),
             py::arg("origCylinder"), py::arg("newCylinder"),
             py::doc(R"pydoc(Creates a ModifiedSolid between the two cylinders)pydoc"))
        .def(py::init<Occ::Solid, 
                      Occ::Solid>(),
             py::arg("origSolid"),
             py::arg("newSolid"), 
             py::doc(
R"mydelimiter(
    Initialized a Modified Solid

    Note that upon instantiation, the ModifiedSolid will be invalid. All of the
    modified/deleted/generated faces must be identified in order to make it valid

    Args:
        origSolid (OccWrapper.Solid): The original, un-modified solid
        newSolid (OccWrapper.Solid): The solid which `origSolid` was modified into.)mydelimiter"))
        .def("__eq__", [](const Occ::ModifiedSolid& s1, const Occ::ModifiedSolid& s2){
                return s1 == s2;}, 
             py::is_operator(),
             py::arg("aModifiedSolid"),
             py::doc("Tests whether self and aModifiedSolid are equal."))
        .def("__ne__", [](const Occ::ModifiedSolid& s1, const Occ::ModifiedSolid& s2){
                return s1 != s2;}, py::is_operator())
        .def("getNewSolid", &Occ::ModifiedSolid::getNewSolid,
             py::doc("returns the new solid"))
        .def("getOrigSolid", &Occ::ModifiedSolid::getOrigSolid,
             py::doc("returns the original solid"))
        .def("addModified", &Occ::ModifiedSolid::addModified,
             py::arg("origFace"), py::arg("newFace"),
             py::doc(R"pydoc(Identifies newFace as a modification of origFace)pydoc"))
        .def("addDeleted", &Occ::ModifiedSolid::addDeleted,
             py::arg("origFace"),
             py::doc(R"pydoc(Identifies origFace as a deleted face)pydoc"))
        .def("addGenerated", &Occ::ModifiedSolid::addGenerated,
             py::arg("newFace"),
             py::doc(R"pydoc(Identifies newFace as a generated face)pydoc"))
        .def("isModified", &Occ::ModifiedSolid::isModified,
             py::arg("origFace"),
             py::doc("Check if `origFace` was deleted"))
        .def("isModification", &Occ::ModifiedSolid::isModification,
             py::arg("newface"),
             py::doc("Check if `newFace` is a modification of an original face."))
        .def("isDeleted", &Occ::ModifiedSolid::isDeleted,
             py::arg("origFace"),
             py::doc("Check if `origFace` was deleted"))
        .def("isGenerated", &Occ::ModifiedSolid::isGenerated,
             py::arg("newFace"),
             py::doc("Check if `newFace` was generated"))
        .def("getModified", &Occ::ModifiedSolid::getModified,
            py::doc(R"pydoc(Returns a list of indices to Face's in myOrigSolid.
These Face's have been modified.)pydoc"))
        .def("getModifiedFace", &Occ::ModifiedSolid::getModifiedFace,
             py::arg("origFace"),
             py::doc(
R"pbdoc(Returns an Occ::ModifiedFace which describes how to get to 
origFace in the resultant BooleanSolid)pbdoc"))
        .def("getDeleted", &Occ::ModifiedSolid::getDeleted,
             py::doc(R"pydoc(Returns a list of indices to faces in the original solid
which have been deleted)pydoc"))
        .def("getGenerated", &Occ::ModifiedSolid::getGenerated,
             py::doc(R"pydoc(Returns a list of indices to faces in the new solid
which have been generaed)pydoc"))
        .doc() = R"pbdoc(
        A ModifiedSolid is a helper class used for communication. It contains
        information regarding a topological update of a solid. This information
        includes which Faces were modified, which were deleted, and which are
        new. It also maintains a copy of the original un-modified solid, as well
        as the new modified solid)pbdoc"
        ;

    //--------------- Define python BoolFaceRef class --------------------------
    py::class_<Occ::BoolFaceRef>(m, "BoolFaceRef")
        .def(py::init<uint, uint, uint>(),
             py::arg("whichMod"), py::arg("whichFace"), py::arg("whichIndex"))
        .def_property_readonly("Mod", &Occ::BoolFaceRef::getModifiedSolidIndex, 
             py::doc(R"py(Which ModifiedSolid)py"))
        .def_property_readonly("Face", &Occ::BoolFaceRef::getSolidFaceIndex, 
             py::doc(R"py(Which Face in the ModifiedSolid::getOrigSolid())py"))
        .def_property_readonly("Index", &Occ::BoolFaceRef::getSplitFaceIndex, 
             py::doc(R"py(Which Modification of Face.)py"))
        .doc() = R"py(This is a simple data class which holds the information needed to fully describe
any given face in a BooleanSolid.)py";

    //------------ Define python BooleanSolid class ---------------------------

    py::class_<Occ::BooleanSolid, Occ::Solid>(m, "BooleanSolid")
        .def_static("makeFusion", &Occ::BooleanSolid::makeFusion,
             py::arg("base"), py::arg("tool"),
             py::doc(R"pbdoc(Args:
    base (OccWrapper.Solid) : The basis for the fusion
    tool (OccWrapper.Solid) : The solid to fuse onto base)pbdoc"))
        .def("getModifiedSolids", &Occ::BooleanSolid::getModifiedSolids,
             py::doc(R"pbdoc(
                Returns the list of `OccWrapper.ModifiedSolid`s which fully
                define the boolean operation which was used to generate this 
                solid.
                 )pbdoc"))
        //.def("getFaceReference", &Occ::BooleanSolid::getFaceReference,
             //py::arg("aFace"),
             //py::doc(R"pbdoc(
                //Finds which face was modified to create `aFace`.

                //Args:
                    //aFace (OccWrapper.Face) : A face in self.getFaces()

                //Returns:
                    //A BoolFaceRef instance.
             //)pbdoc"))
        .doc() = R"py(A BooleanSolid is any solid which is created as the result of a Boolean 
Operation. Specifically, a Boolean Solid can be described by its "Constituent 
Solids" as well how each Face in these Constituent Solids have been modified.)py";
}
