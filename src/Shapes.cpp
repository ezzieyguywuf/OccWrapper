/*
    OccWrapper, a C++ wrapper for OpenCascade.
    Copyright (C) 2018  Wolfgang E. Sanyer. See LICENSE file for more info.
*/
#include "Shapes.hpp"

#include <gp_Trsf.hxx>
#include <BRepAlgoAPI_Cut.hxx>
#include <BRepBuilderAPI_Transform.hxx>
#include <GProp_GProps.hxx>
#include <BRepGProp.hxx>
#include <Precision.hxx>
#include <ShapeAnalysis_Edge.hxx>
#include <TopTools_IndexedMapOfShape.hxx>
#include <TopAbs_ShapeEnum.hxx>
#include <TopExp.hxx>
#include <TopoDS.hxx>

#include <algorithm> // for std::find
#include <sstream>

using Occ::Shape;
using Occ::Edge;
using Occ::Face;
using Occ::Solid;
using std::vector;

/* ========================================================
 *   Occ::Shape definition
 * ========================================================
 */

/*! \class Occ::Shape
 *
 *  Similar to how
 *  [TopoDS_Shape](https://www.opencascade.com/doc/occt-7.3.0/refman/html/class_topo_d_s___shape.html)
 *  functions as a base-class for all other Topological entites, Occ::Shape provides a
 *  base class for all of OpenCascade's various TopoDS_XX entities.
 */
Shape::Shape(TopoDS_Shape aShape)
    : myShape(aShape)
{
    if(myShape.IsNull())
    {
        std::string msg = "Occ::Shape does not support NULL TopoDS_Shape. "
                          "Please pass a valid, non-NULL TopoDS_Shape.";
        throw std::invalid_argument(msg.c_str());
    }
}

Shape::Shape(const std::string& data)
    : Shape(this->shapeFromString(data))
{}

Shape::~Shape()
{}

TopoDS_Shape Shape::shapeFromString(const std::string& data)
{
    std::stringstream stream;
    stream << data;
    BRep_Builder builder;

    TopoDS_Shape shape;
    BRepTools::Read(shape, stream, builder);
    if(shape.IsNull())
    {
        std::string msg = "The string must be in the OpenCasCade BREP format.";
        throw std::invalid_argument(msg.c_str());
    }
    return shape;
}

/*! Utilizes OpenCascade's
 * [TopoDS_Shape.IsEqual()](https://www.opencascade.com/doc/occt-7.3.0/refman/html/class_topo_d_s___shape.html#a0cace3db744fa6361ce3ab39713c9fa5)
 * to check for inequality.
 */
bool Shape::operator==(const Shape& aShape) const
{
    return this->getShape().IsEqual(aShape.getShape());
}

/*! Utilizes OpenCascade's
 * [TopoDS_Shape.IsEqual()](https://www.opencascade.com/doc/occt-7.3.0/refman/html/class_topo_d_s___shape.html#a0cace3db744fa6361ce3ab39713c9fa5)
 * to check for inequality.
 */
bool Shape::operator!=(const Shape& aShape) const
{
    return not (*this == aShape);
}

/*! Geometric overlap is distinct from Topological Equivalence. Realistically, a cube
 * could consist of a single topological Edge, with twelve different orientations. Any two
 * of these topological Edge's (assuming the cube was built this way) would return true
 * using OpenCascade's
 * [TopoDS_Shape::IsPartner()](https://www.opencascade.com/doc/occt-7.3.0/refman/html/class_topo_d_s___shape.html#aa40320323ca3c32355fca293e5802064) 
 * method. In comparison, none of these Edge's would be geometrically equivalent.
 *
 * Take the two faces depicted below. Let's say that the edges in the Original %Face are
 * stored in a vector named `OriginalFace`, and the edges in the Modified %Face are stored
 * in a vector named `ModifiedFace`.
 *
 * ```{.cpp}
 * std::vector<Occ::Edge> OriginalFaces = {E1, E2, E3, E4} // assumes each of E1-E4 is an Occ::Edge
 * std::vector<Occ::Edge> ModifiedFaces = {E1a, E2a, E3, E4, E5, E6, E7}
 *
 * // The purpose for the overlaps method is to figure out what happened to E1 after the
 * // boolean operation
 * for(auto origEdge : OriginalFace){
 *     for(auto modEdge : ModifiedFace){
 *         if (origEdge.overlaps(modEdge)){
 *             // modEdge is probably a "sub edge" of origEdge!
 *         }
 *      }
 *  }
 * ```
 *
 * ```
 *       Original Face
 *
 *             E1
 *     ┌────────────────┐
 *     │                │
 *     │                │
 *  E2 │                │ E3
 *     │                │
 *     │                │
 *     └────────────────┘
 *             E4
 *
 *
 *       Modified Face
 *
 *       E1a        E1b
 *     ┌─────┐    ┌─────┐
 *     │     │    │     │
 *     │   E5│    │E6   │
 *  E2 │     │    │     │ E3
 *     │     └────┘     │
 *     │       E7       │
 *     └────────────────┘
 *             E4
 * ```
 *
 *  Performs a Boolean Cut between `this` and \p aShape, and then a Boolean Cut betwees \p
 *  aShape and `this`. If the resultant volumes are zero, then the two are considered
 *  geometrically equivalent.
 *
 *  Why do we perform two Boolean Cut's? Well, what if `this` is smaller than \p aShape?
 *  Then the first Boolean Cut would result in a volume of zero despite the two being
 *  geomtrically distinct.
 *
 * @param aShape The Occ::Shape to check for overlap
*/
bool Shape::overlaps(const Occ::Shape& aShape) const
{
    BRepAlgoAPI_Cut cut1(this->getShape(), aShape.getShape());
    BRepAlgoAPI_Cut cut2(aShape.getShape(), this->getShape());
    cut1.Build();
    cut2.Build();
    GProp_GProps props1, props2;
    BRepGProp::VolumeProperties(cut1.Shape(), props1, true);
    BRepGProp::VolumeProperties(cut2.Shape(), props2, true);
    return (props1.Mass() <= Precision::Confusion() and props2.Mass() <= Precision::Confusion());
}

/*! Utilizes OpenCascade's
 * [TopoDS_Shape.IsSame()](https://www.opencascade.com/doc/occt-7.3.0/refman/html/class_topo_d_s___shape.html#a143e9ca7835ab15cc933fa52715548d1)
 * so be mindful.
 */
bool Shape::isFlipped(const Shape& aShape) const
{
    return myShape.IsSame(aShape.myShape);
}

bool Shape::isNull() const
{
    return myShape.IsNull();
}

/** Returns a copy - vector will be empty if there aren't any Edge
 *
 *  \see Shape::getShape
 */
vector<Edge> Shape::getEdges() const
{
    vector<Edge> out;

    TopTools_IndexedMapOfShape edges;
    TopExp::MapShapes(this->getShape(), TopAbs_EDGE, edges);
    for (int i=1; i <= edges.Extent(); i++)
    {
        out.push_back(Occ::Edge(TopoDS::Edge(edges.FindKey(i))));
    }
    return out;
}

/** Returns a copy - vector will be empty if there aren't any Face
 *
 *  \see Shape::getShape
 */
vector<Face> Shape::getFaces() const
{
    vector<Face> out;

    TopTools_IndexedMapOfShape faces;
    TopExp::MapShapes(this->getShape(), TopAbs_FACE, faces);
    for (int i=1; i <= faces.Extent(); i++)
    {
        out.push_back(Occ::Face(TopoDS::Face(faces.FindKey(i))));
    }
    return out;
}

/*! Returns a copy. Please note that, as of version 7.3.0, OpenCascade does some
 * interesting stuff in the background regarding smart pointers and memory allocation
 * whenever a copy of a TopoDS_Shape is made. Suffice to say it is worth it to spend a bit
 * of time understanding the implications of working with a copy of a TopoDS_Shape,
 * however in general you should be ok treating the return value of this method normally.
 */
TopoDS_Shape Shape::getShape() const
{
    return myShape;
}

/*! Translate's the shape utilizing
 * [BRepBuilderAPI_Transform()](https://www.opencascade.com/doc/occt-7.3.0/refman/html/class_b_rep_builder_a_p_i___transform.html#a36c59897770510a452e6200e2d714399)
 *
 * @param dx x-direction offset
 * @param dy y-direction offset
 * @param dz z-direction offset
 */
void Shape::translate(double dx, double dy, double dz)
{
    gp_Trsf loc;
    loc.SetTranslationPart(gp_Vec(dx, dy, dz));
    myShape = BRepBuilderAPI_Transform(myShape, loc, true);
}

/*! The resulting file can easily be opened using FreeCAD for inspection.
 * @param filename The name of the output file.
 */
void Shape::writeFile(const std::string filename) const
{
    BRepTools::Write(myShape, filename.c_str());
}

std::string Shape::toString() const
{
    std::ostringstream output;
    BRepTools::Write(this->getShape(), output);
    return output.str();
}

/* ========================================================
 *   Occ::Edge definition
 * ========================================================
 */

Edge::Edge(const TopoDS_Edge& aEdge)
    : Shape(aEdge)
{}

Edge::Edge(const std::string& data)
    : Shape(data)
{
    if(this->getShape().ShapeType() != TopAbs_EDGE)
    {
        std::string msg = "I can only construct TopoDS_Edge";
        throw std::invalid_argument(msg.c_str());
    }
}

/* ========================================================
 *   Occ::Face definition
 * ========================================================
 */

/*! The constructor stores a local copy of each OpenCascade TopDS_Edge as a local copy of
 * an Occ::Edge. This is likely to consume more memory than simply keeping the original
 * TopoDS_Face around, as we may be bypassing some of OpenCascade's built-in memory saving
 * techniques. Then again, since we make a copy of the TopoDS_Edge, perhaps we're still
 * leveraging OpenCascade's memory saving stuff.
 *
 * User be warned.
 */
Face::Face(const TopoDS_Face& aFace)
    : Shape(aFace)
{}

Face::Face(const std::string& data)
    : Shape(data)
{
    if(this->getShape().ShapeType() != TopAbs_FACE)
    {
        std::string msg = "I can only construct TopoDS_Face";
        throw std::invalid_argument(msg.c_str());
    }
}

/*! Uses Occ::Edge::isFlipped(). Due to the specifics of how OpenCascade describes solids,
 * two adjacent %Faces on a particular solid will have a common Topological %Edge, however
 * they will have different Orientations relative to each other.
 */
bool Face::sharesEdge(const Face& aFace) const
{
    for (const Edge& myEdge : this->getEdges())
    {
        for (const Edge& checkEdge : aFace.getEdges())
        {
            if (myEdge.isFlipped(checkEdge))
                return true;
        }
    }
    return false;
}

/*! Uses Occ::Edge::isFlipped() for similar reasons to Occ::Face::sharesEdge()
 */
bool Face::containsEdge(const Edge& anEdge) const
{
    for (const Edge& checkEdge : this->getEdges())
    {
        if (checkEdge.isFlipped(anEdge))
        {
            return true;
        }
    }
    return false;
}

/*! \throws std::runtime_error The user must check that the two faces in question
 * actually share an Edge before invoking this method
 */
Occ::Edge Face::getCommonEdge(const Face& aFace) const
{
    for (const Edge& myEdge : this->getEdges())
    {
        for (const Edge& checkEdge : aFace.getEdges())
        {
            if (myEdge.isFlipped(checkEdge))
                return myEdge;
        }
    }
    throw std::runtime_error("These two Faces do not share an Edge");
}

/* ========================================================
 *   Occ::Solid definition
 * ========================================================
 */

/*! \class Occ::Solid
 *
 * Why do we have a separate Occ::Shape and Occ::Solid class? Well the easy answer is
 * "because OpenCascade did it." Really, though, it does make sense - there are various
 * functionalities that are common to _all_ Topological Entities, regardless of whether or
 * not they just so happen to be a %Solid (i.e. Occ::Shape::writeFile()).
 *
 * Meanwhile, we do still need a way of collecting a set of Occ::Edge and Occ::Face to
 * label them as a "%Solid". That's what this class is for.
 */

// protected
Solid::Solid()
    : Shape(TopoDS_Solid())
{}

Solid::Solid(const TopoDS_Shape& aShape)
    : Shape(aShape)
{
    switch (aShape.ShapeType()) {
    case TopAbs_SOLID:
    case TopAbs_COMPOUND:
        break;
    default:
        throw std::runtime_error("I don't know how to create a solid from that TopoDS_Shape.");
            
    }
}

Solid::Solid(const std::string& data)
    : Shape(data)
{
    // We do this so that we don't have to re-write the check against TopAbs_ShapeEnum
    Solid(this->getShape());
}


Solid::Solid(const TopoDS_Solid& aSolid)
    : Shape(aSolid)
{}

Solid::Solid(const TopoDS_Compound& aCompound)
    : Shape(aCompound)
{}

bool Solid::hasFace(const Occ::Face& aFace) const
{
    for(const Occ::Face& face : this->getFaces())
    {
        if(face == aFace)
            return true;
    }
    return false;
}

/* \throws std::invalid_argument The user must check that aFace is actually a Face in this
 * Solid before invoking this method.
 */
unsigned int Solid::getFaceIndex(const Occ::Face& aFace) const
{
    uint i = 0;
    for (const Occ::Face& myFace : this->getFaces())
    {
        if (myFace.isFlipped(aFace)){
            return i;
        }
        i++;
    }
    throw std::invalid_argument("That face is not in this solid");
}

std::ostream& Occ::operator<<(std::ostream& stream, const Occ::Shape& aShape)
{
    return stream << aShape.toString();
}
