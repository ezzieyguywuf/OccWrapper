- Add support for vertices.
- Generalize how we track Generated faces.
- Install python module in the correct place
    - Maybe have it upload to pip?
- Tests for python module?
- ~~Update Occ::Primitive such that `myNamedFaces` can be made private~~
- Perhaps allow a 'hint' to Occ::Face::getCommonEdge to reduce the amount of looping?
- Refactor Occ::Face::getCommonEdge such that the user doesn't need to check first if the
  two faces share an edge.
- Get rid of Occ::Solid::Solid() constructor, we should not need this.
- eliminate need for user to check for Face existing in Solid in Occ::Solid::getFaceIndex
- provide 'hint' to Occ::Solid::getFaceIndex()

- TODO NOTE: make Occ::Shape a virtual templated base class (change namespace too). Then,
  you can define Occ::Shape which inherits this Other::Shape and implements operator  ==,
  operator !=, isSimilar, and getShape. Perhaps use curiously recurring template pattern?
  Probably won't need to...
 
  Next, Occ::Edge, Occ::Face etc. will also become templated. This will allow us to
  remove all Occ code from OccWrapper, which can be rechristened TopoWrapper.

- Does Occ::Shape::transform need to set Copy = False?
- Write a "what is topology" page for the documentation.
 
 *  Some folks may be confused as to the distinction between a Topological Entity and a
 *  Geometric Entity. Let's consider the triangle depicted below:
 *
 * ```
 *     ⬤
 *     │╲
 *     │ ╲
 *     │  ╲
 *  L1 │   ╲  L3
 *     │    ╲
 *     │     ╲
 *     │      ╲
 *     │       ╲
 *     ⬤───────⬤   
 *        L2
 * ```
 *
 *  Please bear with me, as due to the limitations of ascii art this isosceles right
 *  triangle does not look symettric. Nonetheless, let us assume that the lengths of E1
 *  and E2 are equivalent.
 *
 *  If that's the case, what we have here are _three distinct geometric entities_. Each is
 *  a line-segment with a length (two of which happen to have the same length).
 *
 *  If we were to describe this triangle _topologically_, we could say that we have two
 *  Edges, arranged in a particular manner.
 *
 * ```
 *     ⬤
 *     │╲
 *     │ ╲
 *     │  ╲
 *  E1 │   ╲  E2
 *     │    ╲
 *     │     ╲
 *     │      ╲
 *     │       ╲
 *     ⬤───────⬤   
 *        E1
 * ```
 *
 *  When we discuss "Topology" with regards to geometry, what we're doing is describing
 *  the geometry and how they relate to each other. We don't so much care for the
 *  specifics of the line lengths or vertex locations. This triangle could be described
 *  topologically as "A 'Face' consisting of the arrangement of two 'Edge's".
 *
 *  to be continued...
